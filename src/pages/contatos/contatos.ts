
import { Component, Input, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild, ElementRef } from "@angular/core";
import { NavController, Platform, Content, ModalController, AlertController, LoadingController, Loading, NavParams, Nav } from 'ionic-angular';


import { TokenService, LoginService, StorageService, ErrorService,  UtilService } from '@ons/ons-mobile-login';
import { AnalyticsService } from '@ons/ons-mobile-analytics'
import { StatusBar } from '@ionic-native/status-bar';
import { ContatoService } from "../../services/contatos.service";
//import { UtilService } from "./../../services/util.service";


import { Contato } from '../../models/contato';
import { Gerencia } from '../../models/gerencia';
import { FiltroPage } from '../filtro/filtro';
// import { LoginPage } from '@ons/ons-mobile-login';
import { ContatoPage } from '../contato/contato';


import { HttpClient } from '@angular/common/http';
import { Config } from "./../../environment/config";

import { Subject } from 'rxjs';

import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/interval";
import "rxjs/add/operator/take";
import "rxjs/add/operator/map";
import "rxjs/add/operator/bufferCount"

@Component({
  selector: 'page-contatos',
  templateUrl: 'contatos.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class ContatosPage {

  public exibirSync: boolean = false;
  public RecordForUpdate = false;
  public loading: Loading;
  @ViewChild(Content) content: Content;
  @ViewChild('f2') obj2Input: ElementRef;
  @Input() public contatos: Contato[] = [];
  private contatosCache: Contato[];
  public gerencia: Gerencia = null;
  public filtro: string = '';
  // private unlocked: any = null;
  loadingContato = false;
  loadingGerencia = false;

  ExisteFiltroGerencia: boolean = false;
  PrimeiraCarga = false;
  Tela_Saida_Visivel = false;
  filtrando = false;
  objFiltro: any = {}
  modal: any = {}
  confirma: any = {};
  contadorApenasTeste = 0;

  CargaDadosBackGround: any;

  obsFiltro: any; // = Observable.fromEvent(this.objInput, 'input')
  showSync = new Subject();
  constructor(
    private navCtrl: NavController,
    public platform: Platform,
    private changeDetector: ChangeDetectorRef,
    private service: ContatoService,
    private modalController: ModalController,
    private alertController: AlertController,
    private loadingController: LoadingController,
    private loginSrv: LoginService,
    private storageSrv: StorageService,
    private analyticsService: AnalyticsService,
    // private networkSrv: NetWorkService,
    public navParams: NavParams,
    public statusBar: StatusBar,
    public utilSrv: UtilService,
    public errorSrv: ErrorService,
    private elementRef: ElementRef,
    public tokenSrv: TokenService,
    public nav: Nav,
    public http: HttpClient

  ) {

    console.log('teste eee', tokenSrv.getToken());

    this.statusBar.overlaysWebView(true)
    this.utilSrv.EmAnaliseErro = false;

    this.platform.ready()
      .then(() => {


        this.service.initContatosDB()
          .then(() => {
            console.log('will call contacts')
            var lastTimeBackPress = 0;
            var timePeriodToExit = 3000;

            platform.registerBackButtonAction(() => {
              let view = this.nav.getActive();
              // console.log('view component', view.component.name);

              if ((view.component.name == "ContatosPage")) {
                if (this.filtro !== '') {
                  this.filtro = '';
                  this.utilSrv.ObservableFecharGerencias.next(1)
                  this.getItems('')
                } else {
                  if (this.objFiltro.filtroGerencia !== '') {
                    this.removerFiltro()
                  } else {
                    if (this.filtrando) {
                      this.modal.dismiss();
                      this.navCtrl.pop();
                    } else {
                      if (this.Tela_Saida_Visivel) {
                        this.Tela_Saida_Visivel = false;
                        this.confirma.dismiss();
                      } else {
                        if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
                          this.platform.exitApp(); //Exit from app
                        } else {
                          this.utilSrv.alerta('Pressione o botão voltar novamente\npara desconectar da aplicação.', 4000, 'bottom', 'Atenção!')
                          lastTimeBackPress = new Date().getTime();
                        }
                      }
                    }
                  }
                }
              } else {
                if ((view.component.name == "ContatoPage")) {
                  this.navCtrl.pop();// .setRoot(ContatosPage);
                } else {
                  if ((view.component.name == "LoginPage")) {
                    if (this.platform.is('android')) {
                      if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
                        this.platform.exitApp(); //Exit from app
                      } else {
                        this.utilSrv.alerta('Pressione o botão voltar novamente\npara desconectar da aplicação.', 4000, 'bottom', 'Atenção!')
                        lastTimeBackPress = new Date().getTime();
                      }
                    }
                    else {

                      console.log('saindo em ios')
                    }
                  } else {
                    if ((view.component.name == "FiltroPage")) {
                      // console.log('filtro ativo ', this.filtro)
                      if (this.objFiltro.filtroGerencia !== '') {
                        this.utilSrv.ObservableFecharGerencias.next(true)
                        this.objFiltro.filtroGerencia = ''

                      }
                      else {

                        console.log('saindo em ios')
                      }
                    }
                  }
                }
              }
            });
            //  console.log('get list contatos')

          })

      });
  }

  ionViewDidLoad() {

    this.objFiltro.filtroNome = '';
    this.objFiltro.filtroGerencia = '';
    this.objFiltro.filtroGerenciaExecutiva = '';
    console.log('datat de loginservice ', this.tokenSrv.getToken())
    //debugger;
    if (!this.tokenSrv.isValid(this.tokenSrv.getToken())) {
      this.loginSrv.CurrentUser.Connected = false
      this.loginSrv.logout()
    }
    else {
      //** COmentado na demanda TFS-4632 */
      // this.utilSrv.ForceLoginSetFalse()
      //   .subscribe(data => {

      //   })

      this.cargaInicial();
    }

    //  console.log('Iniciando o interval')
    this.CargaDadosBackGround = Observable.interval(Config.REFRESH_INTERVAL);
    this.CargaDadosBackGround.subscribe(val => {
      if (this.loginSrv.CurrentUser.Connected) {
        this.exibirSync = true;
        if (!this.loadingContato) {
          this.carregarContatosSharePoint();
        } else {
          console.log('Não fez a carga pois já estava em andamento');

        }
      }
    })

    this.showSync.subscribe({
      next: (x) => {
        //  console.log('Show sync')
        this.exibirSync = (this.exibirSync) && (!this.loadingContato && !this.loadingGerencia)
        if (!this.exibirSync) {
          this.PrimeiraCarga = false;
          this.updateRecord(x);
        }
      }
    });

    this.utilSrv.refreshContatos
      .subscribe((gerenciaFiltro: any) => {
        // console.log('gerenciaFiltro => ', gerenciaFiltro)
        if (gerenciaFiltro.GerenciaExecutiva.toLowerCase() !== 'interesses') {
          // this.loadContatos(gerenciaFiltro)
          this.getItemsByGerencia(gerenciaFiltro)
        } else {
          this.getItemsByInteresses(gerenciaFiltro)
        }
      })

  }

  cargaInicial() {
    this.service.getListaContatos()
      .subscribe(
        data => {
          if (data === undefined || data === null || data.length === 0) {
            this.exibirSync = true;
            this.PrimeiraCarga = true;
            this.carregarContatosSharePoint();
          }
          else { this.contatos = data }
        },
        err => {
          this.analyticsService.sendNonFatalCrash("Contatos: Erro: ", err);
        }
      )
  }
  /**
   * Criei para testarmos a atualização dinamica das fotos mas o processo todo ficou muito
   * lento
   *
   * @param {*} contato
   * @returns {string}
   *
   * @memberOf ContatosPage
   */

  getContactPhoto(c: any): string {
    //  console.log('parametro de get photo ', c);

    if ((c.Foto !== null) && (c.Foto !== "")) {
      return c.Foto;
    } else {
      this.service.getContatoPhoto(c.ID)
        .subscribe(
          data => {
            // console.log('peguei a foto de ', c.ID)
            return data; // this.contadorApenasTeste++;
            //   console.log(data)
          },
          err => {
            this.analyticsService.sendNonFatalCrash("getContactPhoto: Erro: ", err);
            //    console.log(err)
          })
    }
  }

  updateRecord(texto) {
    this.RecordForUpdate = false;
    this.service.getListaContatos()
      .subscribe(data => {
        this.contatos = data;
        this.contatosCache = data;
        this.utilSrv.alerta(texto, 4000, 'bottom')
        //    console.log('returned from getListaContatos ', this.contatos);
      });
  }

  SincMessage() {
    this.utilSrv.alerta('Estamos atualizando os dados. \nPode continuar a utilizar o sistema.', 4000, 'bottom', 'Sincronização');
  }

  activatObservables() {
    this.obsFiltro
      .map(event => event.target.value)
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe({
        next: function (value) {
          this.utilSrv.filtroAtivo = value;
          // console.log(this.utilSrv.filtroAtivo)
        }
      });
    this.obsFiltro = Observable.fromEvent(this.elementRef.nativeElement, 'input')
  }
  carregarContatosSharePoint() {
    console.log('token');

    console.log('token ', this.tokenSrv.getToken())
    if (this.loginSrv.CurrentUser.Connected) {
      if (this.tokenSrv.getToken().access_token !== '') {
        this.loadingContato = true;
        // console.log('carregando contatos sharepoint')
        this.storageSrv.recuperar('ultimaAtualizacao')
          .then(dataUltima => {
            //  console.log(dataUltima)
            // console.log('iniciando o get de contatos as ', Date.now.toString())
            this.service.getContatosSharePoint(dataUltima)
              .subscribe(data => {
                // console.log(data)
                if (data[0].Matricula !== '-1') {
                  // console.log('checagem  ' + data[0].Matricula, (data[0].Matricula !== "-1"))
                  this.service.initContatosDB()
                    .then(() => {
                      // console.log('will destroy contacts')
                      this.service.destroyContatosDB(data)
                        .subscribe(d => {
                          this.service.bulkInsertContatosDB(data)
                            .then(
                              data => {
                                //
                                this.loadingContato = false;
                                // console.log('load ok', data)
                                this.showSync.next('Contatos Atualizados');
                                this.updateRecord('')
                                this.carregarGerenciasSharePoint();
                              },
                              err => {
                                this.loadingContato = false;
                                this.exibirSync = false;
                                this.RecordForUpdate = false;
                              })
                        })
                    })
                } else {
                  // console.log('Matricula zerada')
                  let alert = this.alertController.create({
                    title: 'Sistema em manutenção',
                    subTitle: '',
                    buttons: [
                      {
                        text: 'Encerrar',
                        role: 'cancel',
                        handler: () => {
                          this.platform.ready()
                            .then(() => {
                              this.platform.exitApp();
                            })
                        }
                      }

                    ]
                  });
                  alert.present();
                }
              },
                err => {
                  this.utilSrv.alerta('Houve um erro no processo de atualização,\ntentarei novamente em breve.');
                  this.exibirSync = false;
                })
          })
      }
    }
  }

  carregarGerenciasSharePoint() {
    this.loadingGerencia = true;
    // console.log('iniciando carga de gerencias no sharepoint as ', Date.now.toString())
    this.service.getGerenciasSharePoint()
      .subscribe((data) => {
        // console.log('data returned from getgerenciasharepoint ', (data))
        //  console.log('FIM carga de gerencias no sharepoint as ', Date.now.toString())
        this.service.initGerenciaDB()
          .then(() => {
            // console.log('vai apagar gerencias')
            this.service.destroyGerenciasDB()
              .subscribe(d => {
                this.service.bulkInsertGerenciasDB(data)
                  .subscribe(
                    data => {
                      // console.log('load ok', data)
                      this.RecordForUpdate = true;
                      this.loadingGerencia = false;
                      this.showSync.next('Gerencias Atualizadas');
                      this.exibirSync = false;
                      this.PrimeiraCarga = false;
                      this.updateRecord('');
                    },
                    err => {

                      this.utilSrv.alerta('Houve um erro no processo de atualização,\ntentarei novamente em breve.')

                      this.RecordForUpdate = false;
                      this.loadingGerencia = false;
                    })
              })
          })
      })
    // })
    // })
  }

  iniciaCarga() {
    this.loading = this.loadingController.create({
      content: 'Carregando Contatos'
    });

    this.loading.present();
  }


  finalizaCarga() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }
  carregarContatos(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.service.initContatosDB()
        .then(() => {

          if (this.gerencia) {
            this.service.getContatosPorGerencia(this.gerencia)
              .then(data => {
                this.analyticsService.sendCustomEvent("Carregar Contatos", { "Filtro": this.gerencia.Nome });
                this.contatosCache = null;
                this.contatos = data;
                this.changeDetector.markForCheck();
                this.changeDetector.detectChanges();
                resolve();
              })
              .catch((err) => {
                this.analyticsService.sendNonFatalCrash("carregarContatos: Erro: ", err);
                reject(err);
              });
          } else {
            this.service.getContatos()
              .then(data => {
                this.analyticsService.sendCustomEvent("Carregar Contatos", { "Filtro": "Todas" });
                this.contatosCache = null;
                this.contatos = data;
                this.changeDetector.markForCheck();
                this.changeDetector.detectChanges();
                resolve();
              })
              .catch((err) => {
                this.analyticsService.sendNonFatalCrash("carregarContatos: Erro: ", err);
                reject(err);
              });
          }

        });
    });

  }

  loadContatos(filtro?: string) {
    //console.log('carregando contatos', filtro)
    this.service.loadContatos(filtro)
      .subscribe(
        data => {
          this.analyticsService.sendCustomEvent("Carregar Contatos", { "Filtro": "Todas" });
          this.contatosCache = null;
          this.contatos = data;
          this.changeDetector.markForCheck();
          this.changeDetector.detectChanges();
        },
        err => {
          this.analyticsService.sendNonFatalCrash("carregarContatos: Erro: ", err);
          this.utilSrv.alerta('Houve um problema na atualização, \npor favor tente outra vez.')
        })
  }


  showContato(contato: Contato) {
    this.analyticsService.sendContentView(
      contato.Nome,
      "Contato",
      contato._id,
      {
        "Cargo": contato.Cargo,
        "Gerência": contato.Gerencia,
        "Gerência Executiva": contato.GerenciaExecutiva,
        "Localização": contato.Localizacao,
        "Filtro": this.filtro
      }
    );

    this.navCtrl.push(ContatoPage, {
      contato: contato
    });
  }

  enviarEmail(contato: Contato) {
    this.platform.ready().then(() => {
      if (contato.Email) {
        this.analyticsService.sendCustomEvent("Enviar Email", {
          "Email": contato.Email
        });
        window.location.href = 'mailto:' + contato.Email;
      }
    });
  }

  ligar(contato: Contato) {
    this.platform.ready().then(() => {
      if (contato.Telefone) {
        this.analyticsService.sendCustomEvent("Ligar", {
          "Telefone": contato.Telefone
        });
        window.location.href = 'tel:' + contato.Telefone;
      }
    });
  }

  logSwipe(item: any, contato: Contato) {
    this.platform.ready().then(() => {
      let percent = item.getSlidingPercent();
      if (percent == 1) {
        // positive
        this.analyticsService.sendCustomEvent("Viu opções à direita", {
          "Nome": contato.Nome
        });
      } else if (percent == -1) {
        // negative
        this.analyticsService.sendCustomEvent("Viu opções à esquerda", {
          "Nome": contato.Nome
        });
      }
    });
  }


  getItems(ev) {
    // console.log('last test', ev)
    if (!this.contatosCache || this.contatosCache.length == 0) {
      this.contatosCache = this.contatos;
    }
    var val: any;
    // set val to the value of the searchbar
    var t = (typeof ev)
    // console.log(t)
    if (t == 'string') {
      val = ev
    } else {
      val = ev.target.value;
    }

    this.objFiltro.filtroNome = (val == undefined || val == null) ? '' : val;
    val = (val == undefined || val == null) ? '' : val;
    this.contatos = this.contatosCache.filter((contato) => {
      return (
        (contato.Nome.toLowerCase().indexOf(val.toLowerCase()) > -1)
        &&
        this.service.ValidarGerencia(contato, this.objFiltro)
        // ((contato.Gerencia.toLowerCase().trim() === this.objFiltro.filtroGerencia.toLowerCase().trim()) || (this.objFiltro.filtroGerencia == '')
      );
    });
    // console.log('contatos filtrados ', this.contatos)
  }




  getItemsByGerencia(val) {

    if (!this.contatosCache || this.contatosCache.length == 0) {
      this.contatosCache = this.contatos;
    } else {
      this.contatos = this.contatosCache
    }

    val = (val == undefined || val == null) ? '' : val;
    // console.log('nome no retorno para a pesquisa', this.objFiltro.filtroNome)
    // console.log('val em getitensbygerencia ', val);
    this.objFiltro.filtroGerenciaExecutiva = val.GerenciaExecutiva.toLowerCase().trim();
    this.objFiltro.filtroGerencia = val.ID; //.GerenciaExecutiva;
    if (val && val.GerenciaExecutiva.trim() != '') {
      this.contatos = this.contatos.filter((contato) => {
        return (
          ((contato.Gerencia.toLowerCase().trim() === val.ID.toLowerCase().trim()) ||
            (contato.Gerencia.toLowerCase().trim() === val.Nome.toLowerCase().trim()))
          &&
          ((contato.Nome.toLowerCase().indexOf(this.objFiltro.filtroNome.toLowerCase()) > -1) || (this.objFiltro.filtroNome == ''))
        );
      });
      // console.log('contatos filtrados ', this.contatos)
      this.ExisteFiltroGerencia = true;
      this.gerencia = val
    } else {
      this.ExisteFiltroGerencia = false;
      this.contatos = this.contatosCache;
    }
    //this.changeDetector.detectChanges();
  }

  getItemsByInteresses(val) {

    if (!this.contatosCache || this.contatosCache.length == 0) {
      this.contatosCache = this.contatos;
    } else {
      this.contatos = this.contatosCache
    }

    val = (val == undefined || val == null) ? '' : val;
    // console.log('nome no retorno para a pesquisa', this.objFiltro.filtroNome)
    // console.log('val em getitensbygerencia ', val);
    // console.log('contatos ', this.contatos);


    this.objFiltro.filtroGerencia = val.ID; //.GerenciaExecutiva;
    this.objFiltro.filtroGerenciaExecutiva = val.GerenciaExecutiva.toLowerCase().trim();
    if (val && val.GerenciaExecutiva.trim() != '') {
      this.contatos = this.contatos.filter((contato) => {
        // console.log('find ', contato.Interesse.toLowerCase().trim().indexOf(val.ID.toLowerCase().trim()));

        return (
          (contato.Interesse.toLowerCase().trim().indexOf(val.ID.toLowerCase().trim()) !== -1)
          //(contato.Interesse.toLowerCase().trim().match(val.ID) !== null)
          &&
          ((contato.Nome.toLowerCase().indexOf(this.objFiltro.filtroNome.toLowerCase()) > -1) || (this.objFiltro.filtroNome == ''))
        );
      })

      // console.log('contatos filtrados ', this.contatos)
      this.ExisteFiltroGerencia = true;
      this.gerencia = val
    } else {
      this.ExisteFiltroGerencia = false;
      this.contatos = this.contatosCache;
    }
    //this.changeDetector.detectChanges();
  }

  sair() {
    this.showConfirm();
  }

  filtrar() {
    this.filtrando = true;
    this.analyticsService.sendCustomEvent("Abriu opções de Filtros");

    this.modal = this.modalController.create(FiltroPage);
    //  console.log('modal =>', modal)
    this.modal.onDidDismiss((data) => {
      //  console.log('dado do filtro ', data);
      this.filtrando = false;
      if (data) {
        this.analyticsService.sendCustomEvent("Selecionou Filtro", {
          "Gerência": data.gerencia.Nome
        });
        this.navCtrl.setRoot(ContatosPage, {
          gerencia: data.gerencia
        });
      } else {
        this.analyticsService.sendCustomEvent("Desistiu de Selecionar Filtros");
      }
    });

    this.modal.present();
  }

  removerFiltro() {
    this.analyticsService.sendCustomEvent("Removeu Filtro Selecionado");
    this.gerencia = null;
    this.ExisteFiltroGerencia = false;
    this.objFiltro.filtroGerencia = '';
    // console.log('filtro em removerFiltro ', this.objFiltro)

    this.getItems(this.objFiltro.filtroNome)


  }

  showConfirm() {
    this.confirma = this.alertController.create({
      title: 'Opções de saída',
      message: 'Você pode Sair e entrar facilmente na próxima vez ou Encerrar, removendo seus dados de login.',
      buttons: [
        {
          text: 'Sair',
          handler: () => {
            this.loginSrv.logout()
          }
        },
        {
          text: 'Encerrar',
          handler: () => {
            this.loginSrv.exitApp()
          }
        },
        {
          text: 'Cancelar',
          handler: () => {
            this.Tela_Saida_Visivel = false;
          }
        }
      ]
    });
    this.Tela_Saida_Visivel = true;
    this.confirma.present();
  }
}
