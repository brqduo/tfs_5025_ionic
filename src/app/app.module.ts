import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicImageViewerModule } from 'ionic-img-viewer';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ContatosPage } from '../pages/contatos/contatos';
import { ContatoPage } from '../pages/contato/contato';
import { FiltroPage } from '../pages/filtro/filtro';
import { Firebase } from '@ionic-native/firebase';
import { ModalImageComponent } from '../components/modal-image/modal-image';

import {
  OnsPackage,
  LoginService,
  TokenService,
  ErrorService,
  LoginPage,
  UserService,
  StorageService,
  ImageService,
  // SecurityService,
  NetWorkService,
  UtilService
} from '@ons/ons-mobile-login'

import {
  AnalyticsService,
  OnsAnalyticsModule
} from '@ons/ons-mobile-analytics'
import { HttpClientModule } from '@angular/common/http';
import { ContatoService } from '../services/contatos.service';
import { PushService } from '../services/push.service';
import { Config } from '../environment/config';
import { JWT_OPTIONS, JwtModule } from '@auth0/angular-jwt';
import { Device } from '@ionic-native/device';
import { AppVersion } from '@ionic-native/app-version';


// const config = Config.FIREBASECONFIG;

export function jwtOptionsFactory(tokenSrv: TokenService) {
  return {
    tokenGetter: () => {
      let tk = tokenSrv.getToken();
      console.log("Token: ", tk);
      return tk.access_token;
    },
    whitelistedDomains: Config.WHITELISTEDDOMAINS
  }
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ContatosPage,
    ContatoPage,
    FiltroPage,
    ModalImageComponent
  ],
  imports: [
    BrowserModule,
    OnsAnalyticsModule.forRoot(),
    OnsPackage.forRoot(),
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: 'Voltar'
    }),
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: (jwtOptionsFactory),
        deps: [LoginService]
      }
    }),
    IonicImageViewerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ContatosPage,
    ContatoPage,
    FiltroPage,
    LoginPage,
    ModalImageComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ContatoService,
    UtilService,
    //AnalyticsPackage
    AnalyticsService,
    UtilService,
    LoginService,
    ErrorService,
    UserService,
    StorageService,
    ImageService,
    // SecurityService,
    NetWorkService,
    // Firebase
    Device,
    PushService,
    AppVersion,
    Firebase,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
