import { StorageService, UtilService } from '@ons/ons-mobile-login';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Contato } from '../models/contato';
import { Gerencia } from '../models/gerencia';
import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
import cordovaSqlitePlugin from 'pouchdb-adapter-cordova-sqlite';
import { AnalyticsService } from '@ons/ons-mobile-analytics'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from '../environment/config';
import { Device } from '@ionic-native/device';
import { AppVersion } from '@ionic-native/app-version';


const header_default = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

const httpOptionsDefault = {
  headers: header_default
};

// const header_json = {
//   'Content-Type':'application/json'};

// const httpOptionsJson = {
//   headers: header_json
// }

@Injectable()
export class ContatoService {

  private contatosDB;
  private gerenciasDB;
  private contatos: Contato[];
  private gerencias: Gerencia[];
  private appv: any;


  constructor(
    private platform: Platform,
    public utilSrv: UtilService,
    private httpClient: HttpClient,
    public storageSrv: StorageService,
    private analyticsService: AnalyticsService,
    private device: Device,
    private appVersion: AppVersion) {


  }

  private GetDbAdapter() {
    if (this.platform.is('cordova')) {
      return 'websql';//'cordova-sqlite';
    } else {
      return 'websql';
    }
  }

  private setUltimaAtualizacao() {
    this.storageSrv.Gravar('ultimaAtualizacao', new Date())
      .then(
        data => {
          // console.log('returning data ', data)
          return data;
        },
        err => {
          this.analyticsService.sendNonFatalCrash("storage.get(ultimaAtualizacao)", err);
          return new Date(0)
        })
  }



  initContatosDB(): Promise<any> {

    return new Promise((resolve, reject) => {
      PouchDB.plugin(cordovaSqlitePlugin);
      PouchDB.plugin(PouchDBFind);
      this.contatosDB = new PouchDB('contatos.db', { adapter: this.GetDbAdapter() });
      resolve();
    });
  }
  destroyContatosDB(listaContatos): Observable<any> {
    var arrayDb = []
    var teste = [];
    // this.reinitContatosDB();
    listaContatos.forEach(element => {
      arrayDb.push(element.ID)
    });
    // console.log('Array db', arrayDb)
    return Observable.fromPromise(this.contatosDB.allDocs({ include_docs: true })
      .then(docs => {
        docs.rows.forEach(element => {
          if (arrayDb.indexOf(element.doc.ID) !== -1) {
            teste.push({ _id: element.id, _rev: element.doc._rev, _deleted: true });
          }
        });
        // console.log('Array de exclusoes', teste)
        this.contatosDB.bulkDocs(teste);
      })
    );
  }

  destroyGerenciasDB(): Observable<any> {
    var teste = [];
    //this.reinitGerenciasDB();
    return Observable.fromPromise(this.gerenciasDB.allDocs({ include_docs: true })
      .then(docs => {
        // console.log('destroy gerencia ', docs)
        docs.rows.forEach(element => {
          teste.push({ _id: element.id, _rev: element.doc._rev, _deleted: true });
        });
        //    console.log('Array de gerencias exclusoes', teste)
        this.gerenciasDB.bulkDocs(teste);
      })
    )
    // return Observable.of('ok');
  }


  OinitContatosDB(): Observable<any> {
    return Observable.fromPromise(this.initContatosDB())
  }


  reinitContatosDB(): Promise<any> {
    return new Promise((resolve, reject) => {
      PouchDB.plugin(cordovaSqlitePlugin);
      PouchDB.plugin(PouchDBFind);

      return new PouchDB('contatos.db', { adapter: this.GetDbAdapter() })
        .destroy()
        .then(() => {
          return new PouchDB('contatos.db', { adapter: this.GetDbAdapter() });
        })
        .then((ContatosDB) => {
          this.contatosDB = ContatosDB;
          resolve();
        }).catch((error) => {
          this.analyticsService.sendNonFatalCrash("reinitContatosDB()", error);
          reject(error);
        });
    });
  }

  public reinitGerenciasDB(): Promise<any> {
    return new Promise((resolve, reject) => {
      PouchDB.plugin(cordovaSqlitePlugin);
      PouchDB.plugin(PouchDBFind);

      return new PouchDB('gerencias.db', { adapter: this.GetDbAdapter() })
        .destroy()
        .then(() => {
          return new PouchDB('gerencias.db', { adapter: this.GetDbAdapter() });
        })
        .then((GerenciasDB) => {
          this.gerenciasDB = GerenciasDB;
          resolve();
        }).catch((error) => {
          this.analyticsService.sendNonFatalCrash("reinitGerenciasDB()", error);
          reject(error);
        });
    });
  }

  initGerenciaDB(): Promise<any> {
    return new Promise((resolve, reject) => {
      PouchDB.plugin(cordovaSqlitePlugin);

      try {
        this.gerenciasDB = new PouchDB('gerencias.db', { adapter: this.GetDbAdapter() });
        resolve();
      } catch (error) {
        this.analyticsService.sendNonFatalCrash("initGerenciaDB()", error);
        reject(error);
      }
    });
  }



  loadContatos(filtro): Observable<any> {
    this.OinitContatosDB();
    return Observable.fromPromise(this.contatosDB.allDocs({ include_docs: true })
      .then(docs => {
        this.contatos = docs.rows.map(row => {
          if ((row.doc.Nome === filtro) || (filtro = undefined) || (filtro == null)) {
            return row.doc;
          }
        });
        this.contatos = this.contatos.sort(this.ordernarPorNome);
        return this.contatos;
      })
    );
  }

  getListaContatos(): Observable<Contato[]> {
    this.initContatosDB()
    // console.log('fazendo a carga de base inicial ');
    console.log("Inicia Contatos");
    return Observable.fromPromise(

      this.contatosDB.allDocs({ include_docs: true })
        .then(docs => {
          console.log('docs lidos de contatos ', docs)
          this.contatos = docs.rows.map(row => {
            return row.doc;
          });
          this.contatos = this.contatos.sort(this.ordernarPorNome);
          return this.contatos;
        })
    )
  }

  getContatoPhoto(id: string): Observable<any> {
    const httpOptions = { headers: new HttpHeaders({ "responseType": 'json' }) };
    return this.httpClient.get(`${Config.INTRANET_URL}/fotos?id=` + id, httpOptions)
      .map(data => {
        return data
      });
  }


  registrarFirebase() {

    // const headers =  new HttpHeaders({   'Content-Type':  'application/x-www-form-urlencoded' });
    //  const options = {headers};

    this.appVersion.getVersionNumber().then((x: string) => {
      this.appv = x;
    })

    const body = {
      "appVersao": this.appv,
      "dispositivoId": this.device.uuid,
      "dispositivoDetalhe": {
        "so": this.device.platform,
        "modelo": this.device.model
      },
      "topicos": [
        "Publicações"
      ]
    };

    console.log(JSON.stringify(body));


    this.httpClient.post("http://localhost:51078/api/pushnotifications/registrar", body, httpOptionsDefault)
      .subscribe(data => {
        console.log('Registro', data);
        return data;
      });
    //   .((err:Response) => {
    //     let details = err.json();
    //     console.log('Detalhes do erro:', details);
    //     return Observable.throw(details);
    //  });
  }

  getContatos(): Promise<Contato[]> {
    if (!this.contatos) {
      return this.contatosDB.allDocs({ include_docs: true })
        .then(docs => {
          this.contatos = docs.rows.map(row => {
            return row.doc;
          });

          this.contatos = this.contatos.sort(this.ordernarPorNome);

          return this.contatos;
        });
    } else {
      this.contatos = this.contatos.sort(this.ordernarPorNome);
      return Promise.resolve(this.contatos);
    }
  }

  getContatosPorGerencia(gerencia: Gerencia): Promise<Contato[]> {

    if (!this.contatos) {
      return this.contatosDB.find({ selector: { Gerencia: gerencia.Nome } })
        .then((result) => {
          this.contatos = result.docs;

          return this.contatos.sort(this.ordernarPorNome);
        });
    } else {
      let retorno = this.contatos.filter(function (data) {
        return data.Gerencia == gerencia.Nome;
      });
      return Promise.resolve(retorno.sort(this.ordernarPorNome));
    }
  }

  getGerencias(): Observable<Gerencia[]> {
    //   if (!this.gerencias) {
    return Observable.fromPromise(this.gerenciasDB.allDocs({ include_docs: true })
      .then(
        docs => {
          this.gerencias = docs.rows.map(row => {
            return row.doc;
          });
          this.gerencias = this.gerencias.sort(this.ordernarPorNome);
          return this.gerencias;
        })
      .catch(err => { this.analyticsService.sendNonFatalCrash("getGerencias()", err); })
    );
  }

  private ordernarPorNome(a, b) {
    if (a.Nome < b.Nome)
      return -1;
    if (a.Nome > b.Nome)
      return 1;
    return 0;
  }

  getContatosSharePoint(dataAtualizacao): Observable<any> {
    // console.log('httpOptions  ', httpOptions)
    if (dataAtualizacao == null || dataAtualizacao == undefined) {
      dataAtualizacao = new Date(0);
    }

    return this.httpClient.get(`${Config.INTRANET_URL}/modificados?data=2018-01-01&tipo=completo`, httpOptionsDefault)
      .map((data: string) => {
        this.setUltimaAtualizacao()
        // console.log('dados do sharepoint', data)
        return data
      },
        err => {
          this.analyticsService.sendNonFatalCrash("getContatosSharePoint()", err);
          this.utilSrv.alerta(Config.MensagemGenerica.MENSAGEM_ALERTA_CONTATOS_SHAREPOINT, 5000, 'top', 'Leitura de contatos');
          return false;
        }
      );
  }


  getGerenciasSharePoint() {

    return this.httpClient.get(`${Config.INTRANET_URL}/Gerencias`, httpOptionsDefault)
      .map((data: Gerencia[]) => {
        return data;
      });

  }


  bulkInsertContatosDB(contatos: Contato[]): Promise<any> {
    return new Promise((resolve, reject) => {
      var that = this;
      // console.log('contatos DB on insert ', this.contatosDB)
      // console.log(contatos)
      return this.contatosDB.bulkDocs(contatos)
        .then((x) => {
          that.contatosDB.allDocs(function (err, response) {
            if (err) {
              // console.log('erro de insert de contatos ', err)
              that.analyticsService.sendNonFatalCrash("bulkInsertContatosDB()", err);
              return err
            } else {
              return x
            }

          });
          resolve();
        }).catch((error) => {
          this.analyticsService.sendNonFatalCrash("insertTmpContatosDB()", error);
          reject(error);
        });
    });
  }

  bulkInsertGerenciasDB(gerencias: Gerencia[]): Observable<any> {
    return Observable.fromPromise(new Promise((resolve, reject) => {
      return this.gerenciasDB.bulkDocs(gerencias)
        .then((x) => {
          resolve();
        }).catch((error) => {
          this.analyticsService.sendNonFatalCrash("insertGerenciasDB()", error);
          reject(error);
        });
    })
    )
  }

  clearAll() {
    this.reinitContatosDB()
      .then(() => {
        this.analyticsService.sendCustomEvent("Clear Contatos");
        this.reinitGerenciasDB()
          .then(() => {
            this.analyticsService.sendCustomEvent("Clear Gerencia");
            this.storageSrv.limparBase();
            this.analyticsService.sendCustomEvent("Clear Storage");
          })
      })
  }

  ValidarGerencia(contato: Contato, objFiltro): boolean {
    var result = false;
    // console.log('objFiltro ', objFiltro);
    if (objFiltro.filtroGerenciaExecutiva == 'interesses') {
      // console.log('contato ', contato);

      // console.log('log ', contato.Interesse.toLowerCase().trim().indexOf(objFiltro.filtroGerencia.toLowerCase().trim()));
      result = (contato.Interesse.toLowerCase().trim().indexOf(objFiltro.filtroGerencia.toLowerCase().trim()) !== -1)
    } else {
      result = ((contato.Gerencia.toLowerCase().trim() === objFiltro.filtroGerencia.toLowerCase().trim()) || (objFiltro.filtroGerencia == ''))
    }
    return result;
  }

  getImagemPerfil(id: string) {
    return new Promise(resolve => {
      this.httpClient.get(`${Config.INTRANET_URL}/getcontatoimage?id=` + id, { responseType: 'text' })
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log(err);
        });
    });

  }
}
