webpackJsonp([0],{

/***/ 189:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 189;

/***/ }),

/***/ 232:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 232;

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\brq\TFS_5025\src\pages\home\home.html"*/'<!-- <ion-header>\n\n  <ion-navbar>\n\n    <ion-title>\n\n      Ionic Blank\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n\n\n\n\n<ion-content padding>\n\n  The world is your oyster.\n\n  <p>\n\n    If you get lost, the <a href="http://ionicframework.com/docs/v2">docs</a> will be your guide.\n\n  </p>\n\n  <button ion-button icon-only>\n\n    <ion-icon name="home"></ion-icon>\n\n  </button>\n\n</ion-content> -->'/*ion-inline-end:"C:\brq\TFS_5025\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 370:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContatosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ons_ons_mobile_login__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ons_ons_mobile_analytics__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_contatos_service__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__filtro_filtro__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__contato_contato__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_common_http__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__environment_config__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_add_observable_interval__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_add_observable_interval___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_rxjs_add_observable_interval__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs_add_operator_take__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs_add_operator_take___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_rxjs_add_operator_take__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_rxjs_add_operator_map__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_rxjs_add_operator_bufferCount__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_rxjs_add_operator_bufferCount___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_rxjs_add_operator_bufferCount__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







// import { LoginPage } from '@ons/ons-mobile-login';









var ContatosPage = /** @class */ (function () {
    function ContatosPage(navCtrl, platform, changeDetector, service, modalController, alertController, loadingController, loginSrv, storageSrv, analyticsService, 
        // private networkSrv: NetWorkService,
        navParams, statusBar, utilSrv, errorSrv, elementRef, tokenSrv, nav, http) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.changeDetector = changeDetector;
        this.service = service;
        this.modalController = modalController;
        this.alertController = alertController;
        this.loadingController = loadingController;
        this.loginSrv = loginSrv;
        this.storageSrv = storageSrv;
        this.analyticsService = analyticsService;
        this.navParams = navParams;
        this.statusBar = statusBar;
        this.utilSrv = utilSrv;
        this.errorSrv = errorSrv;
        this.elementRef = elementRef;
        this.tokenSrv = tokenSrv;
        this.nav = nav;
        this.http = http;
        this.exibirSync = false;
        this.RecordForUpdate = false;
        this.contatos = [];
        this.gerencia = null;
        this.filtro = '';
        // private unlocked: any = null;
        this.loadingContato = false;
        this.loadingGerencia = false;
        this.ExisteFiltroGerencia = false;
        this.PrimeiraCarga = false;
        this.Tela_Saida_Visivel = false;
        this.filtrando = false;
        this.objFiltro = {};
        this.modal = {};
        this.confirma = {};
        this.contadorApenasTeste = 0;
        this.showSync = new __WEBPACK_IMPORTED_MODULE_10_rxjs__["Subject"]();
        console.log('teste eee', tokenSrv.getToken());
        this.statusBar.overlaysWebView(true);
        this.utilSrv.EmAnaliseErro = false;
        this.platform.ready()
            .then(function () {
            _this.service.initContatosDB()
                .then(function () {
                console.log('will call contacts');
                var lastTimeBackPress = 0;
                var timePeriodToExit = 3000;
                platform.registerBackButtonAction(function () {
                    var view = _this.nav.getActive();
                    // console.log('view component', view.component.name);
                    if ((view.component.name == "ContatosPage")) {
                        if (_this.filtro !== '') {
                            _this.filtro = '';
                            _this.utilSrv.ObservableFecharGerencias.next(1);
                            _this.getItems('');
                        }
                        else {
                            if (_this.objFiltro.filtroGerencia !== '') {
                                _this.removerFiltro();
                            }
                            else {
                                if (_this.filtrando) {
                                    _this.modal.dismiss();
                                    _this.navCtrl.pop();
                                }
                                else {
                                    if (_this.Tela_Saida_Visivel) {
                                        _this.Tela_Saida_Visivel = false;
                                        _this.confirma.dismiss();
                                    }
                                    else {
                                        if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
                                            _this.platform.exitApp(); //Exit from app
                                        }
                                        else {
                                            _this.utilSrv.alerta('Pressione o botão voltar novamente\npara desconectar da aplicação.', 4000, 'bottom', 'Atenção!');
                                            lastTimeBackPress = new Date().getTime();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        if ((view.component.name == "ContatoPage")) {
                            _this.navCtrl.pop(); // .setRoot(ContatosPage);
                        }
                        else {
                            if ((view.component.name == "LoginPage")) {
                                if (_this.platform.is('android')) {
                                    if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
                                        _this.platform.exitApp(); //Exit from app
                                    }
                                    else {
                                        _this.utilSrv.alerta('Pressione o botão voltar novamente\npara desconectar da aplicação.', 4000, 'bottom', 'Atenção!');
                                        lastTimeBackPress = new Date().getTime();
                                    }
                                }
                                else {
                                    console.log('saindo em ios');
                                }
                            }
                            else {
                                if ((view.component.name == "FiltroPage")) {
                                    // console.log('filtro ativo ', this.filtro)
                                    if (_this.objFiltro.filtroGerencia !== '') {
                                        _this.utilSrv.ObservableFecharGerencias.next(true);
                                        _this.objFiltro.filtroGerencia = '';
                                    }
                                    else {
                                        console.log('saindo em ios');
                                    }
                                }
                            }
                        }
                    }
                });
                //  console.log('get list contatos')
            });
        });
    }
    ContatosPage_1 = ContatosPage;
    ContatosPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.objFiltro.filtroNome = '';
        this.objFiltro.filtroGerencia = '';
        this.objFiltro.filtroGerenciaExecutiva = '';
        console.log('datat de loginservice ', this.tokenSrv.getToken());
        //debugger;
        if (!this.tokenSrv.isValid(this.tokenSrv.getToken())) {
            this.loginSrv.CurrentUser.Connected = false;
            this.loginSrv.logout();
        }
        else {
            //** COmentado na demanda TFS-4632 */
            // this.utilSrv.ForceLoginSetFalse()
            //   .subscribe(data => {
            //   })
            this.cargaInicial();
        }
        //  console.log('Iniciando o interval')
        this.CargaDadosBackGround = __WEBPACK_IMPORTED_MODULE_11_rxjs_Observable__["Observable"].interval(__WEBPACK_IMPORTED_MODULE_9__environment_config__["a" /* Config */].REFRESH_INTERVAL);
        this.CargaDadosBackGround.subscribe(function (val) {
            if (_this.loginSrv.CurrentUser.Connected) {
                _this.exibirSync = true;
                if (!_this.loadingContato) {
                    _this.carregarContatosSharePoint();
                }
                else {
                    console.log('Não fez a carga pois já estava em andamento');
                }
            }
        });
        this.showSync.subscribe({
            next: function (x) {
                //  console.log('Show sync')
                _this.exibirSync = (_this.exibirSync) && (!_this.loadingContato && !_this.loadingGerencia);
                if (!_this.exibirSync) {
                    _this.PrimeiraCarga = false;
                    _this.updateRecord(x);
                }
            }
        });
        this.utilSrv.refreshContatos
            .subscribe(function (gerenciaFiltro) {
            // console.log('gerenciaFiltro => ', gerenciaFiltro)
            if (gerenciaFiltro.GerenciaExecutiva.toLowerCase() !== 'interesses') {
                // this.loadContatos(gerenciaFiltro)
                _this.getItemsByGerencia(gerenciaFiltro);
            }
            else {
                _this.getItemsByInteresses(gerenciaFiltro);
            }
        });
    };
    ContatosPage.prototype.cargaInicial = function () {
        var _this = this;
        this.service.getListaContatos()
            .subscribe(function (data) {
            if (data === undefined || data === null || data.length === 0) {
                _this.exibirSync = true;
                _this.PrimeiraCarga = true;
                _this.carregarContatosSharePoint();
            }
            else {
                _this.contatos = data;
            }
        }, function (err) {
            _this.analyticsService.sendNonFatalCrash("Contatos: Erro: ", err);
        });
    };
    /**
     * Criei para testarmos a atualização dinamica das fotos mas o processo todo ficou muito
     * lento
     *
     * @param {*} contato
     * @returns {string}
     *
     * @memberOf ContatosPage
     */
    ContatosPage.prototype.getContactPhoto = function (c) {
        //  console.log('parametro de get photo ', c);
        var _this = this;
        if ((c.Foto !== null) && (c.Foto !== "")) {
            return c.Foto;
        }
        else {
            this.service.getContatoPhoto(c.ID)
                .subscribe(function (data) {
                // console.log('peguei a foto de ', c.ID)
                return data; // this.contadorApenasTeste++;
                //   console.log(data)
            }, function (err) {
                _this.analyticsService.sendNonFatalCrash("getContactPhoto: Erro: ", err);
                //    console.log(err)
            });
        }
    };
    ContatosPage.prototype.updateRecord = function (texto) {
        var _this = this;
        this.RecordForUpdate = false;
        this.service.getListaContatos()
            .subscribe(function (data) {
            _this.contatos = data;
            _this.contatosCache = data;
            _this.utilSrv.alerta(texto, 4000, 'bottom');
            //    console.log('returned from getListaContatos ', this.contatos);
        });
    };
    ContatosPage.prototype.SincMessage = function () {
        this.utilSrv.alerta('Estamos atualizando os dados. \nPode continuar a utilizar o sistema.', 4000, 'bottom', 'Sincronização');
    };
    ContatosPage.prototype.activatObservables = function () {
        this.obsFiltro
            .map(function (event) { return event.target.value; })
            .debounceTime(500)
            .distinctUntilChanged()
            .subscribe({
            next: function (value) {
                this.utilSrv.filtroAtivo = value;
                // console.log(this.utilSrv.filtroAtivo)
            }
        });
        this.obsFiltro = __WEBPACK_IMPORTED_MODULE_11_rxjs_Observable__["Observable"].fromEvent(this.elementRef.nativeElement, 'input');
    };
    ContatosPage.prototype.carregarContatosSharePoint = function () {
        var _this = this;
        console.log('token');
        console.log('token ', this.tokenSrv.getToken());
        if (this.loginSrv.CurrentUser.Connected) {
            if (this.tokenSrv.getToken().access_token !== '') {
                this.loadingContato = true;
                // console.log('carregando contatos sharepoint')
                this.storageSrv.recuperar('ultimaAtualizacao')
                    .then(function (dataUltima) {
                    //  console.log(dataUltima)
                    // console.log('iniciando o get de contatos as ', Date.now.toString())
                    _this.service.getContatosSharePoint(dataUltima)
                        .subscribe(function (data) {
                        // console.log(data)
                        if (data[0].Matricula !== '-1') {
                            // console.log('checagem  ' + data[0].Matricula, (data[0].Matricula !== "-1"))
                            _this.service.initContatosDB()
                                .then(function () {
                                // console.log('will destroy contacts')
                                _this.service.destroyContatosDB(data)
                                    .subscribe(function (d) {
                                    _this.service.bulkInsertContatosDB(data)
                                        .then(function (data) {
                                        //
                                        _this.loadingContato = false;
                                        // console.log('load ok', data)
                                        _this.showSync.next('Contatos Atualizados');
                                        _this.updateRecord('');
                                        _this.carregarGerenciasSharePoint();
                                    }, function (err) {
                                        _this.loadingContato = false;
                                        _this.exibirSync = false;
                                        _this.RecordForUpdate = false;
                                    });
                                });
                            });
                        }
                        else {
                            // console.log('Matricula zerada')
                            var alert_1 = _this.alertController.create({
                                title: 'Sistema em manutenção',
                                subTitle: '',
                                buttons: [
                                    {
                                        text: 'Encerrar',
                                        role: 'cancel',
                                        handler: function () {
                                            _this.platform.ready()
                                                .then(function () {
                                                _this.platform.exitApp();
                                            });
                                        }
                                    }
                                ]
                            });
                            alert_1.present();
                        }
                    }, function (err) {
                        _this.utilSrv.alerta('Houve um erro no processo de atualização,\ntentarei novamente em breve.');
                        _this.exibirSync = false;
                    });
                });
            }
        }
    };
    ContatosPage.prototype.carregarGerenciasSharePoint = function () {
        var _this = this;
        this.loadingGerencia = true;
        // console.log('iniciando carga de gerencias no sharepoint as ', Date.now.toString())
        this.service.getGerenciasSharePoint()
            .subscribe(function (data) {
            // console.log('data returned from getgerenciasharepoint ', (data))
            //  console.log('FIM carga de gerencias no sharepoint as ', Date.now.toString())
            _this.service.initGerenciaDB()
                .then(function () {
                // console.log('vai apagar gerencias')
                _this.service.destroyGerenciasDB()
                    .subscribe(function (d) {
                    _this.service.bulkInsertGerenciasDB(data)
                        .subscribe(function (data) {
                        // console.log('load ok', data)
                        _this.RecordForUpdate = true;
                        _this.loadingGerencia = false;
                        _this.showSync.next('Gerencias Atualizadas');
                        _this.exibirSync = false;
                        _this.PrimeiraCarga = false;
                        _this.updateRecord('');
                    }, function (err) {
                        _this.utilSrv.alerta('Houve um erro no processo de atualização,\ntentarei novamente em breve.');
                        _this.RecordForUpdate = false;
                        _this.loadingGerencia = false;
                    });
                });
            });
        });
        // })
        // })
    };
    ContatosPage.prototype.iniciaCarga = function () {
        this.loading = this.loadingController.create({
            content: 'Carregando Contatos'
        });
        this.loading.present();
    };
    ContatosPage.prototype.finalizaCarga = function () {
        if (this.loading) {
            this.loading.dismiss();
        }
    };
    ContatosPage.prototype.carregarContatos = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.service.initContatosDB()
                .then(function () {
                if (_this.gerencia) {
                    _this.service.getContatosPorGerencia(_this.gerencia)
                        .then(function (data) {
                        _this.analyticsService.sendCustomEvent("Carregar Contatos", { "Filtro": _this.gerencia.Nome });
                        _this.contatosCache = null;
                        _this.contatos = data;
                        _this.changeDetector.markForCheck();
                        _this.changeDetector.detectChanges();
                        resolve();
                    })
                        .catch(function (err) {
                        _this.analyticsService.sendNonFatalCrash("carregarContatos: Erro: ", err);
                        reject(err);
                    });
                }
                else {
                    _this.service.getContatos()
                        .then(function (data) {
                        _this.analyticsService.sendCustomEvent("Carregar Contatos", { "Filtro": "Todas" });
                        _this.contatosCache = null;
                        _this.contatos = data;
                        _this.changeDetector.markForCheck();
                        _this.changeDetector.detectChanges();
                        resolve();
                    })
                        .catch(function (err) {
                        _this.analyticsService.sendNonFatalCrash("carregarContatos: Erro: ", err);
                        reject(err);
                    });
                }
            });
        });
    };
    ContatosPage.prototype.loadContatos = function (filtro) {
        var _this = this;
        //console.log('carregando contatos', filtro)
        this.service.loadContatos(filtro)
            .subscribe(function (data) {
            _this.analyticsService.sendCustomEvent("Carregar Contatos", { "Filtro": "Todas" });
            _this.contatosCache = null;
            _this.contatos = data;
            _this.changeDetector.markForCheck();
            _this.changeDetector.detectChanges();
        }, function (err) {
            _this.analyticsService.sendNonFatalCrash("carregarContatos: Erro: ", err);
            _this.utilSrv.alerta('Houve um problema na atualização, \npor favor tente outra vez.');
        });
    };
    ContatosPage.prototype.showContato = function (contato) {
        this.analyticsService.sendContentView(contato.Nome, "Contato", contato._id, {
            "Cargo": contato.Cargo,
            "Gerência": contato.Gerencia,
            "Gerência Executiva": contato.GerenciaExecutiva,
            "Localização": contato.Localizacao,
            "Filtro": this.filtro
        });
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__contato_contato__["a" /* ContatoPage */], {
            contato: contato
        });
    };
    ContatosPage.prototype.enviarEmail = function (contato) {
        var _this = this;
        this.platform.ready().then(function () {
            if (contato.Email) {
                _this.analyticsService.sendCustomEvent("Enviar Email", {
                    "Email": contato.Email
                });
                window.location.href = 'mailto:' + contato.Email;
            }
        });
    };
    ContatosPage.prototype.ligar = function (contato) {
        var _this = this;
        this.platform.ready().then(function () {
            if (contato.Telefone) {
                _this.analyticsService.sendCustomEvent("Ligar", {
                    "Telefone": contato.Telefone
                });
                window.location.href = 'tel:' + contato.Telefone;
            }
        });
    };
    ContatosPage.prototype.logSwipe = function (item, contato) {
        var _this = this;
        this.platform.ready().then(function () {
            var percent = item.getSlidingPercent();
            if (percent == 1) {
                // positive
                _this.analyticsService.sendCustomEvent("Viu opções à direita", {
                    "Nome": contato.Nome
                });
            }
            else if (percent == -1) {
                // negative
                _this.analyticsService.sendCustomEvent("Viu opções à esquerda", {
                    "Nome": contato.Nome
                });
            }
        });
    };
    ContatosPage.prototype.getItems = function (ev) {
        var _this = this;
        // console.log('last test', ev)
        if (!this.contatosCache || this.contatosCache.length == 0) {
            this.contatosCache = this.contatos;
        }
        var val;
        // set val to the value of the searchbar
        var t = (typeof ev);
        // console.log(t)
        if (t == 'string') {
            val = ev;
        }
        else {
            val = ev.target.value;
        }
        this.objFiltro.filtroNome = (val == undefined || val == null) ? '' : val;
        val = (val == undefined || val == null) ? '' : val;
        this.contatos = this.contatosCache.filter(function (contato) {
            return ((contato.Nome.toLowerCase().indexOf(val.toLowerCase()) > -1)
                &&
                    _this.service.ValidarGerencia(contato, _this.objFiltro));
        });
        // console.log('contatos filtrados ', this.contatos)
    };
    ContatosPage.prototype.getItemsByGerencia = function (val) {
        var _this = this;
        if (!this.contatosCache || this.contatosCache.length == 0) {
            this.contatosCache = this.contatos;
        }
        else {
            this.contatos = this.contatosCache;
        }
        val = (val == undefined || val == null) ? '' : val;
        // console.log('nome no retorno para a pesquisa', this.objFiltro.filtroNome)
        // console.log('val em getitensbygerencia ', val);
        this.objFiltro.filtroGerenciaExecutiva = val.GerenciaExecutiva.toLowerCase().trim();
        this.objFiltro.filtroGerencia = val.ID; //.GerenciaExecutiva;
        if (val && val.GerenciaExecutiva.trim() != '') {
            this.contatos = this.contatos.filter(function (contato) {
                return (((contato.Gerencia.toLowerCase().trim() === val.ID.toLowerCase().trim()) ||
                    (contato.Gerencia.toLowerCase().trim() === val.Nome.toLowerCase().trim()))
                    &&
                        ((contato.Nome.toLowerCase().indexOf(_this.objFiltro.filtroNome.toLowerCase()) > -1) || (_this.objFiltro.filtroNome == '')));
            });
            // console.log('contatos filtrados ', this.contatos)
            this.ExisteFiltroGerencia = true;
            this.gerencia = val;
        }
        else {
            this.ExisteFiltroGerencia = false;
            this.contatos = this.contatosCache;
        }
        //this.changeDetector.detectChanges();
    };
    ContatosPage.prototype.getItemsByInteresses = function (val) {
        var _this = this;
        if (!this.contatosCache || this.contatosCache.length == 0) {
            this.contatosCache = this.contatos;
        }
        else {
            this.contatos = this.contatosCache;
        }
        val = (val == undefined || val == null) ? '' : val;
        // console.log('nome no retorno para a pesquisa', this.objFiltro.filtroNome)
        // console.log('val em getitensbygerencia ', val);
        // console.log('contatos ', this.contatos);
        this.objFiltro.filtroGerencia = val.ID; //.GerenciaExecutiva;
        this.objFiltro.filtroGerenciaExecutiva = val.GerenciaExecutiva.toLowerCase().trim();
        if (val && val.GerenciaExecutiva.trim() != '') {
            this.contatos = this.contatos.filter(function (contato) {
                // console.log('find ', contato.Interesse.toLowerCase().trim().indexOf(val.ID.toLowerCase().trim()));
                return ((contato.Interesse.toLowerCase().trim().indexOf(val.ID.toLowerCase().trim()) !== -1)
                    //(contato.Interesse.toLowerCase().trim().match(val.ID) !== null)
                    &&
                        ((contato.Nome.toLowerCase().indexOf(_this.objFiltro.filtroNome.toLowerCase()) > -1) || (_this.objFiltro.filtroNome == '')));
            });
            // console.log('contatos filtrados ', this.contatos)
            this.ExisteFiltroGerencia = true;
            this.gerencia = val;
        }
        else {
            this.ExisteFiltroGerencia = false;
            this.contatos = this.contatosCache;
        }
        //this.changeDetector.detectChanges();
    };
    ContatosPage.prototype.sair = function () {
        this.showConfirm();
    };
    ContatosPage.prototype.filtrar = function () {
        var _this = this;
        this.filtrando = true;
        this.analyticsService.sendCustomEvent("Abriu opções de Filtros");
        this.modal = this.modalController.create(__WEBPACK_IMPORTED_MODULE_6__filtro_filtro__["a" /* FiltroPage */]);
        //  console.log('modal =>', modal)
        this.modal.onDidDismiss(function (data) {
            //  console.log('dado do filtro ', data);
            _this.filtrando = false;
            if (data) {
                _this.analyticsService.sendCustomEvent("Selecionou Filtro", {
                    "Gerência": data.gerencia.Nome
                });
                _this.navCtrl.setRoot(ContatosPage_1, {
                    gerencia: data.gerencia
                });
            }
            else {
                _this.analyticsService.sendCustomEvent("Desistiu de Selecionar Filtros");
            }
        });
        this.modal.present();
    };
    ContatosPage.prototype.removerFiltro = function () {
        this.analyticsService.sendCustomEvent("Removeu Filtro Selecionado");
        this.gerencia = null;
        this.ExisteFiltroGerencia = false;
        this.objFiltro.filtroGerencia = '';
        // console.log('filtro em removerFiltro ', this.objFiltro)
        this.getItems(this.objFiltro.filtroNome);
    };
    ContatosPage.prototype.showConfirm = function () {
        var _this = this;
        this.confirma = this.alertController.create({
            title: 'Opções de saída',
            message: 'Você pode Sair e entrar facilmente na próxima vez ou Encerrar, removendo seus dados de login.',
            buttons: [
                {
                    text: 'Sair',
                    handler: function () {
                        _this.loginSrv.logout();
                    }
                },
                {
                    text: 'Encerrar',
                    handler: function () {
                        _this.loginSrv.exitApp();
                    }
                },
                {
                    text: 'Cancelar',
                    handler: function () {
                        _this.Tela_Saida_Visivel = false;
                    }
                }
            ]
        });
        this.Tela_Saida_Visivel = true;
        this.confirma.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Content */])
    ], ContatosPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('f2'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], ContatosPage.prototype, "obj2Input", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Array)
    ], ContatosPage.prototype, "contatos", void 0);
    ContatosPage = ContatosPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contatos',template:/*ion-inline-start:"C:\brq\TFS_5025\src\pages\contatos\contatos.html"*/'<ion-header style="">\n\n	<ion-navbar color="primary_80" class="headerSeparado">\n\n		<ion-buttons left>\n\n			<button ion-button icon-only color="primary_20" (click)="sair()">\n\n				<ion-icon name="log-out"></ion-icon>\n\n			</button>\n\n		</ion-buttons>\n\n		<ion-title>Contatos</ion-title>\n\n		<ion-buttons right>\n\n			<button ion-button icon-only *ngIf="exibirSync" ion-button small="true" color="primary_20" (click)="SincMessage()">\n\n				<ion-spinner name="bubbles" icon="ios-small"></ion-spinner>\n\n			</button>\n\n		</ion-buttons>\n\n	</ion-navbar>\n\n	<ion-toolbar>\n\n		<ion-searchbar #f2 placeholder="Procurar..." [(ngModel)]="filtro" (ionInput)="getItems($event)"></ion-searchbar>\n\n		<ion-buttons end>\n\n			<button class="filtroContato" ion-button icon-only (click)="filtrar()">\n\n				<ion-icon name="funnel" [color]="gerencia ? \'secondary_40\' : \'primary_80\'"></ion-icon>\n\n			</button>\n\n		</ion-buttons>\n\n	</ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n	<ion-card *ngIf="PrimeiraCarga">\n\n		<ion-card-header>\n\n			CARGA INCIAL\n\n		</ion-card-header>\n\n		<ion-card-content>\n\n			Estamos processando a\n\n			<br>primeira carga dos dados\n\n			<br> de contatos. Por favor, aguarde\n\n			<br> alguns instantes.\n\n		</ion-card-content>\n\n	</ion-card>\n\n	<ul [hidden]="!ExisteFiltroGerencia" class="gerenciaFilter" style="height: 40px;" (click)="removerFiltro()" tappable>\n\n		<li>\n\n			{{gerencia ? gerencia.Nome : ""}}\n\n			<button ion-button icon-only>\n\n				<ion-icon name="close"></ion-icon>\n\n			</button>\n\n		</li>\n\n	</ul>\n\n	<ion-item *ngIf="contatos.length === 0 && filtro !== \'\'">\n\n		<!-- <img src=\'assets/icon/alert-red.png\'> -->\n\n		<ion-label>\n\n			<b>{{filtro}}</b> não encontrado(a)\n\n		</ion-label>\n\n		<!-- <ion-img src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCACfAH8DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD2SiiitSwoooJwMmgAziopJ4okLu6qo7k1xV54yfUtYew0ba1vB/rrr+HPoPWor/UW25dyzdhUSqJGkKTkdLdeJLW2BKqz/pWcfG0A/wCWH/j/AP8AWrjp3lmbLnr0qu8X41g676HVHCxtqejWniqzuWw6tHnv94VtRTRzxh4nV1PdTXjSPLA+5CR6it7S9akiYMjlG7jsauFa+5lUw9tj0qiuXt/F9tFdRQX2IllOEk7Z9DXUdRntW6dzmaaCkpaKAEopaKAHUUUUAFcN8SNdls9Ni0exbF5fnYSOqx9/z6fnXcE4FeOzTvrnxAub12zFb/Kg7ADgf596mcrIqEeaVjTs7OHRdMhs4gNwGXP95qTG9tzHk1NNmW4IqxHaFQDXnzm2z06cUkVPIVjyKY9qCPetLy8H0pjQMTx07GpNrmPLakdqqOhjfIBB9RW80Pvms67gYDcBxT1JepC8EeqWMlnMfvD5T3B7Guh+GniKbULG50XUH3X+nttBPV4+x/Dp+Vc1E5idT0NUba9Gh/EOw1FG2wz/ALuf0Kng/wCP4V00pnFXp9T26ilorqOMSilooAKKWigRW1GUwabcyr1WJiPrivI9AQILiUdZH6+1eq63J5WiXjnnETV5h4fj3Qtk4KnkYrGu/dN6HxG5bRb23d62wiCHpgDvWKkixt6VcM4aPAfAxXHE9AnbZjtUbcYqrnrg9KGdulMokPOM4AqleeVsYA9akcM3SqVz8ucnmmLZmZKMN9KwNfUkQyf3XrfkfcelYOtylWjhI+V6KaakZ1vhPc9JuReaRZ3IOfNgR/zWrlYvhFt3hXT/APZj2/kSP6VtV6C2PMCig0UALSClooEUNbZE0W78zJUxFcD34ry7QS8c10sqNHz8u7+leleJVZ/Dt4ifeKDH/fQrgoUDX12xQFfLQAe43VzVnrY66Efd5iHUdRMTeTbrum7k9FrldR1LVkYsmoRxj0yM/wDoVaGq2NzOfs9vI8ETcyMG5Pt7Vg3HhRHnjmh/doo+cZBLfnWUGjWak9i/pl/qsrB/7VMyqeV3A12ttdPJBvdgCOMHjmuKtrRG1J/LttvGRtGB+Jrp4rSNbQtNCDI3zHPO3jpUyZpTTSsZera1dl2itpPLP94Vyn2zVZbjbLrMQP8AtTAVt6jZO7Tb/MZezA7t3tWCdD/tKWHI8pYl27kGN1XFpIzqRk3ob1jLeQOpknS4gbHzo24VY8S2wiexckOm8HK91xn+lZEuiSWd7FPbEw8AFUGEOPWugzJPaxea7Zjccsc/Ljp9OlNNXJcZNHpHgOdJvDEIXOUdlYHsev8AWulrl/Arg6PcRhcbbhjn1yAa6muqEuaKZyVI8smhKMUfjRVEi0UUUCKeqjOk3Yxn90xx+FebaKjmK6M+RMrbXB7dxXqUyCWF0IyGUrivP44vIEiEYbPPrXLX3TOzDP3WiOO0VgWQc+9UTYEMQdoXPata3kVZMt09Kz9T1BYidgyx4ArkO+K0IfKVCIYkRFY/M5OTU0pxFhRgAYrLJhtx5148u5jwqKSc+nFalveWs1vu+fqPvLtIH0NWkGiZiOCsodSNx+Ug9GFOFmBN5yR7H/2WqO9uLb7RsZZCC3BC5AoinNvKACdh9aewtGaMtq8savK33egFV2RVtJgw+6hbip5bksOvBpFAYY9RjFCepMo6HXfD1nbRZ96FWE2Of90V19c74Mi2aIzEYLysf0Aroa7qatBHl1nebCiiirMwzRRRQIK57xFaRRiOeONVZ3IdgOvFdDVTUrX7ZYyQj7/VPrUVI80WaUp8sjz+ZiGwKzljImeaXBfOFz2rUnXDelUby2S6bYSdp64NeclZnqc11oNKM3Vcj6ZqnNbFnz86npmhbBoZBmR2T/afn9aqyG5VmVJyP+A//XrRaj5b9SSS18rG1T+NRoGP3xwOlUJrW4kbhi7HknGP51ZtrVreVN8zuuOcmmyWrM0SuAK1/DunpqOrRwyLuhALSDpxisjeHOR90V6F4T0s2VgbmVcSz8j2XtTpQvIzr1OWJuwQRW0CwwoEjUYCipKKK7jzW7hRRSUALRRRQIKKKKAPPtUXytQuo/SRsfnWUjYbHetTWWJ1afd/H86/Tp/SsORirllNedKNptM9SErwTRoqvygsAfeqk1rEZM7BzVdtQMa7WBxVV9WjZ/vFcUrNGilHqXXt4wDtXFU3O1SKjfUQ4wpz61DvMjjJ/Cq5epMpLoaWlRG6v7aADiSVV/WvYAABgcCvK/DoKatDMB8sBDN/n869UByK6qEXy3OLEy96wtFJRWxzBRRRmgYtFQyzxQLmRwPQdzVJ9UY/6mIH/fNWoSexLkkaZOByeKzZ9Ws2Z7aKdXmx0XnA781l6lfTTwOjvtXH3UHvisLw4rNcXk7vna/lAn25P8x+RraFHqzOVTsa2o2CX1vsJ2SLykn90/4djXGXizW101vcp5c3p2ceqnvXffkD7/5/D8qgvNNttUtTb3Ue5f4DnDKfr2I9aivhlUV1ozWhiHT0exwTwMVypzVOVpoyR5Wa172zutCl8u7JmtHOIrkL09n9D79KqXQDAEOoQjrmvJlCcJcskepGUZK8WZx3kEvxU9pbzXEoit03zN37L9a07Dw9cXm2W4321r/CCMSSfQfwj3NdLb2kFpCIoI1SMdh3/H+tddHCynrLRHLVxMYaR1ZBY2KWVssSnc33nf8AvN/nj866zT9Stmt4opJ0WYDGGOM1gEVSv1+QNu249OlelKknFJHn+0d7s77qKK4bTdXvrOQQ7vMj7AjI/wDrV0dvr1vJgTK0TevUVzSpSRopJmtSZpkc8Uy7opFdfUGn1FmUcrJOGc7iS2Tkn2GTS+flDs4Pc/8AASahJwWP/XQ/0qVkyzL/ALWP/HQv9a7kznZHOMxMfQr/AOhGq2jRGKORVXln3fVsf15FX0TKjPfY3/jxqvap5d06AbRIvzH3DHaaaJexfBGOOh6E+nb/AANSx9+Dz29f/riouvb1JH/oS/1riPiJ4vOiWI0yyf8A025X55Af9Wn94H+8w/z0ok7LUFqxvirx7ClzJpum7ZtmVnuNu4Z/ur6n1Ncnb+OLm11CGdNOtRBEcSIsf3gfRv4TXHadKY2VW+ePPyhicfSp5iVWVhgL3AP+c1lpJXNU2tD3q11O31exivLV/MjlA6dc/wB38O9SAkkYI56Ht7n6CvDvD3iW/wDDV6sux3sZx+8j7Mv95fRvevZ9P1G01SxS8tHElvIOcdRjonsfWtISUtCGmi1uAXqQMZ9wv+JqtdI0vlxbVy0gDZ6A+n4VaOQ3ZmDfm/8AgKjVN9xCP4EPX+8ecmqbEiBIA5344659cvUjgxzN3Uu3H/AhVhUxGv8AuR/+hU2QblP1f/0MVJQ5JXjO5VwyhjkHHRsVfh1e5jkEZAkG5l+YelUB/q2J7q3/AKHUqLm5Hs7/ANKhpMaY1V3RqPZB+ZyakB3SBu2S36k/0FMiZvKBHUuQM+wwP51IqhEPsr/pxVEhGfmX6RinFMTI4+ZdjK49QTQABL9HUfktRiYhUKnDBUI/Fqa3BhqN59ity4+eZuEHq3ZvpjrXD6v4US9DXN6N0kjs5J6muxJj817mRd0pUhF7IobgCszxFdPb2Elx1IZlVfwx/jRLUI6HB+JdMtrPRLJIkjidZwUAHtzXKQRR3erWsV3sSF5Rv7DHp+Nb3iG5lmMFvJybWIljnu2P6Yrnrp2LRttBEajgccA/4mpaGj0a10iC+tWW4hQxuiqqY4UbuMVW0rS9R8MatNNZu0untuZ4focA/UetdDYMslnbsgwGijb8zmtaKEfN7o//AKFRYCaKRJ4g8bYUqQD3Ve+fc1JEWMzZGFDoAP7oweP1pTbKkxMeF3SZZezYHekhVwzZbJd0bNW3dCS1FJ/dD/cX9GoAzu+pP/j4owSg9gP/AEKlPCsfZ/0YGpGDDajf7j/+hVNAMzD3d/6VHP8Axr6b6lh4mX/fb/0GkNH/2Q=="></ion-img> -->\n\n	</ion-item>\n\n	<ion-list [virtualScroll]="contatos" [approxItemHeight]="\'60px\'" [bufferRatio]="4">\n\n		<ion-item-sliding *virtualItem="let contato" (ionDrag)="logSwipe($event, contato)">\n\n\n\n			<button ion-item (click)="showContato(contato)">\n\n				<ion-avatar item-left [hidden]="contato.Foto == null">\n\n					<figure>\n\n						<img src={{contato.Foto}}>\n\n					</figure>\n\n				</ion-avatar>\n\n				<ion-avatar item-left [hidden]="contato.Foto != null">\n\n					<ion-icon name="contact" class="avatarGrande"></ion-icon>\n\n				</ion-avatar>\n\n				<h2>{{contato.Nome}}</h2>\n\n				<p item-content>\n\n					{{contato.Gerencia}}\n\n				</p>\n\n				<p>{{contato.Telefone}}</p>\n\n			</button>\n\n			<ion-item-options side="left">\n\n				<button ion-button (click)="enviarEmail(contato)">\n\n					<ion-icon name="mail"></ion-icon>E-mail\n\n				</button>\n\n			</ion-item-options>\n\n			<ion-item-options side="right">\n\n				<button ion-button (click)="ligar(contato)">\n\n					<ion-icon name="call"></ion-icon>Ligar\n\n				</button>\n\n			</ion-item-options>\n\n		</ion-item-sliding>\n\n	</ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\brq\TFS_5025\src\pages\contatos\contatos.html"*/,
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* ChangeDetectionStrategy */].Default
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["j" /* ChangeDetectorRef */],
            __WEBPACK_IMPORTED_MODULE_5__services_contatos_service__["a" /* ContatoService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__ons_ons_mobile_login__["e" /* LoginService */],
            __WEBPACK_IMPORTED_MODULE_2__ons_ons_mobile_login__["h" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_3__ons_ons_mobile_analytics__["a" /* AnalyticsService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_2__ons_ons_mobile_login__["k" /* UtilService */],
            __WEBPACK_IMPORTED_MODULE_2__ons_ons_mobile_login__["b" /* ErrorService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */],
            __WEBPACK_IMPORTED_MODULE_2__ons_ons_mobile_login__["i" /* TokenService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Nav */],
            __WEBPACK_IMPORTED_MODULE_8__angular_common_http__["b" /* HttpClient */]])
    ], ContatosPage);
    return ContatosPage;
    var ContatosPage_1;
}());

//# sourceMappingURL=contatos.js.map

/***/ }),

/***/ 388:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FiltroPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_contatos_service__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ons_ons_mobile_login__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_interval__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_interval___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_interval__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_take__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_take___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_take__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_bufferCount__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_bufferCount___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_bufferCount__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
 //, NgZone, Input



//import { UtilService } from './../../services/util.service';








// import { Nav } from 'ionic-angular';
var FiltroPage = /** @class */ (function () {
    function FiltroPage(viewController, platform, changeDetector, service, elementRef, utilSrv //,
        // public nav: Nav
    ) {
        this.viewController = viewController;
        this.platform = platform;
        this.changeDetector = changeDetector;
        this.service = service;
        this.elementRef = elementRef;
        this.utilSrv = utilSrv; //,
        this.gerencias = [];
        this.that = this;
        //  console.log('entrei em filtro', this.objInput)
        this.obsFiltro = __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].fromEvent(this.elementRef.nativeElement, 'input');
        this.carregarGerencias();
    }
    FiltroPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var that = this;
        this.obsFiltro
            .map(function (event) { return event.target.value; })
            .debounceTime(500)
            .distinctUntilChanged()
            .subscribe({
            next: function (value) {
                // console.log(value);
                that.utilSrv.filtroAtivo = value;
                that.getItems(value);
            }
        });
        this.utilSrv.ObservableFecharGerencias
            .subscribe(function (data) {
            // console.log('teste de exit do filtro');
            var u;
            _this.objInputBar.clearInput(u);
            _this.filtro = ''; // this.LimparFiltro('');
        });
    };
    FiltroPage.prototype.carregarGerencias = function () {
        var _this = this;
        this.service.initGerenciaDB()
            .then(function () {
            _this.service.getGerencias()
                .subscribe(function (data) {
                _this.gerencias = [];
                data.forEach(function (element) {
                    if (element.ID !== '') {
                        _this.gerencias.push(element);
                    }
                });
                _this.changeDetector.markForCheck();
            }, function (error) {
                console.error.bind(console);
            });
        });
    };
    FiltroPage.prototype.getItems = function (ev) {
        if (!this.gerenciasCache || this.gerenciasCache.length == 0) {
            this.gerenciasCache = this.gerencias;
        }
        var val = ev + '';
        this.gerencias = this.gerenciasCache.filter(function (gerencia) {
            return ((gerencia.Nome.toLowerCase().indexOf(val.toLowerCase()) > -1) || (gerencia.GerenciaExecutiva.toLowerCase().indexOf(val.toLowerCase()) > -1) || (val == ''));
        });
        this.changeDetector.detectChanges();
    };
    FiltroPage.prototype.filtrarGerencia = function (gerencia) {
        // console.log('gerencia no filtrar => ', gerencia)
        this.utilSrv.refreshContatos.next(gerencia);
        this.viewController.dismiss();
    };
    FiltroPage.prototype.fechar = function () {
        this.viewController.dismiss();
    };
    FiltroPage.prototype.LimparFiltro = function (ev) {
        if ((ev.target.value === '') || (ev.target.value === undefined) || (ev.target.value === null)) {
            this.carregarGerencias();
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Content */])
    ], FiltroPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('filtro_bar'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], FiltroPage.prototype, "objInput", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('filtro_bar'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* Searchbar */])
    ], FiltroPage.prototype, "objInputBar", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Array)
    ], FiltroPage.prototype, "gerencias", void 0);
    FiltroPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-filtro',template:/*ion-inline-start:"C:\brq\TFS_5025\src\pages\filtro\filtro.html"*/'<ion-header>\n\n  <ion-navbar color="primary_80">\n\n    <ion-title>Gerências e Interesses</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (click)="fechar()">\n\n        <ion-icon name="close"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n  <ion-toolbar>\n\n    <ion-searchbar #filtro_bar placeholder="Procurar..." [(ngModel)]="filtro" (ionInput)="LimparFiltro($event)"></ion-searchbar>\n\n  </ion-toolbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-list [virtualScroll]="gerencias" [approxItemHeight]="\'60px\'" [bufferRatio]="4" [hidden]="gerencias.length == 0">\n\n    <ion-item-sliding *virtualItem="let gerencia">\n\n      <button ion-item (click)="filtrarGerencia(gerencia)" detail-none>\n\n        <ion-avatar item-left>\n\n          <ion-badge item-end>{{utilSrv.padLeft(gerencia.GerenciaCount,\'0\',2)}}</ion-badge>\n\n        </ion-avatar>\n\n        <h2>{{gerencia.Nome}}</h2>\n\n        <p>{{gerencia.GerenciaExecutiva}}</p>\n\n      </button>\n\n    </ion-item-sliding>\n\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\brq\TFS_5025\src\pages\filtro\filtro.html"*/,
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* ChangeDetectionStrategy */].OnPush
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["y" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["j" /* ChangeDetectorRef */],
            __WEBPACK_IMPORTED_MODULE_2__services_contatos_service__["a" /* ContatoService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */],
            __WEBPACK_IMPORTED_MODULE_3__ons_ons_mobile_login__["k" /* UtilService */] //,
            // public nav: Nav
        ])
    ], FiltroPage);
    return FiltroPage;
}());

//# sourceMappingURL=filtro.js.map

/***/ }),

/***/ 389:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContatoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__environment_config__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_contatos_service__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_analytics__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_modal_image_modal_image__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ons_ons_mobile_login__ = __webpack_require__(39);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







//import { Navbar } from 'ionic-angular/navigation/nav-interfaces';
var ContatoPage = /** @class */ (function () {
    function ContatoPage(viewCtrl, platform, navParams, service, changeDetector, navCtrl, modalCtrl, networkSrv, 
        // private contatoSrv: ContatoService,
        analyticsService) {
        this.viewCtrl = viewCtrl;
        this.platform = platform;
        this.navParams = navParams;
        this.service = service;
        this.changeDetector = changeDetector;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.networkSrv = networkSrv;
        this.analyticsService = analyticsService;
        this.contato = {};
        this.contatosMesmaGerencia = [];
        this.background = 'assets/img/background/background-8.jpg';
        this.Interesses = [];
    }
    ContatoPage_1 = ContatoPage;
    ContatoPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var c = this.navParams.get('contato');
        if (c) {
            this.contato = c;
            this.carregarContatosMesmaGerencia();
        }
        else {
            this.dismiss();
        }
        this.Interesses = this.contato.Interesse.split(__WEBPACK_IMPORTED_MODULE_0__environment_config__["a" /* Config */].delimiterInteresses);
        // console.log('contato ', this.contato);
        // console.log('interesse ', this.Interesses);
        this.platform.ready().then(function () {
            _this.networkSrv.updateNetworkStatus();
            console.log(_this.networkSrv.isNetworkConnected());
        });
    };
    ContatoPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss(this.contato);
    };
    ContatoPage.prototype.carregarContatosMesmaGerencia = function () {
        var _this = this;
        this.service.initContatosDB()
            .then(function () {
            if (_this.contato.Gerencia) {
                var gerencia = { ID: "", Nome: _this.contato.Gerencia, GerenciaExecutiva: "", GerenciaCount: 0 };
                _this.service.getContatosPorGerencia(gerencia)
                    .then(function (data) {
                    _this.contatosMesmaGerencia = data.filter(function (contato) {
                        return contato.Nome.toLowerCase().indexOf(_this.contato.Nome.toLowerCase()) == -1;
                    });
                    _this.changeDetector.markForCheck();
                })
                    .catch(console.error.bind(console));
            }
            else {
                _this.contatosMesmaGerencia = [];
            }
            _this.content.resize();
        });
    };
    ContatoPage.prototype.enviarEmail = function (contato) {
        var _this = this;
        this.platform.ready().then(function () {
            if (contato.Email) {
                _this.analyticsService.sendCustomEvent("Enviar Email", {
                    "Email": contato.Email
                });
                window.location.href = 'mailto:' + contato.Email;
            }
        });
    };
    ContatoPage.prototype.ligar = function (contato) {
        var _this = this;
        this.platform.ready().then(function () {
            if (contato.Telefone) {
                _this.analyticsService.sendCustomEvent("Ligar", {
                    "Telefone": contato.Telefone
                });
                window.location.href = 'tel:' + contato.Telefone;
            }
        });
    };
    ContatoPage.prototype.whatsapp = function (contato) {
        var _this = this;
        this.platform.ready().then(function () {
            if (contato.Celular) {
                _this.analyticsService.sendCustomEvent("Ligar", {
                    "Celular": contato.Celular
                });
                // window.open('tel:' + contato.Celular, '_system');
                window.location.href = 'tel:' + contato.Celular;
            }
        });
    };
    ContatoPage.prototype.showContato = function (contato) {
        this.analyticsService.sendContentView(contato.Nome, "Contato Mesma Gerência", contato._id, {
            "Cargo": contato.Cargo,
            "Gerência": contato.Gerencia,
            "Gerência Executiva": contato.GerenciaExecutiva,
            "Localização": contato.Localizacao
        });
        this.navCtrl.push(ContatoPage_1, {
            contato: contato
        });
    };
    ContatoPage.prototype.abrirImagemPerfil = function (id) {
        var _this = this;
        var IsConnected = false;
        this.platform
            .ready()
            .then(function () {
            IsConnected = _this.networkSrv.isNetworkConnected().connected;
            if (IsConnected) {
                var imgPerfilModal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__components_modal_image_modal_image__["a" /* ModalImageComponent */], { id: id }, { cssClass: 'select-modal' });
                imgPerfilModal.present();
            }
            else {
                var imgPerfilModal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__components_modal_image_modal_image__["a" /* ModalImageComponent */], { id: id, notConnected: __WEBPACK_IMPORTED_MODULE_0__environment_config__["a" /* Config */].imgNotConnected }, { cssClass: 'select-modal' });
                imgPerfilModal.present();
            }
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Content */])
    ], ContatoPage.prototype, "content", void 0);
    ContatoPage = ContatoPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-contato',template:/*ion-inline-start:"C:\brq\TFS_5025\src\pages\contato\contato.html"*/'<ion-header>\n\n  <ion-navbar color="primary_80">\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content parallax-header no-bounce class="bg-modal">\n\n  <div class="header-image" [ngStyle]="{\'background-image\': \'url(\' + background +\')\'}"></div>\n\n  <div class="main-content" text-wrap text-center>\n\n    <figure class="perfil">\n\n      <img src="{{contato.Foto}}" [hidden]="!contato.Foto" (click)="abrirImagemPerfil(contato.ID)">\n\n      <ion-icon name="contact" class="avatarHeader" [hidden]="contato.Foto"></ion-icon>\n\n    </figure>\n\n    <h4 class="list-item-nome-1">{{contato.Nome}}</h4>\n\n    <p class="list-item-cargo">\n\n      {{contato.Cargo}} &bull; {{contato.Gerencia}} &bull; {{contato.GerenciaExecutiva}}\n\n    </p>\n\n    <hr>\n\n\n\n\n\n    <ion-list no-lines>\n\n      <!--<ion-item class="contact-info">\n\n        <ion-icon item-start name="ion-pound" color="orange_80"></ion-icon>\n\n        <a href="#" class="ion_link">{{contato.Matricula}}</a>\n\n      </ion-item>-->\n\n      <ion-item class="contact-info" [hidden]="!contato.Email">\n\n        <ion-icon name="mail" color="orange_80" (click)="enviarEmail(contato)" item-start></ion-icon>\n\n        <a href="#" class="ion_link" (click)="enviarEmail(contato)">{{contato.Email}}</a>\n\n      </ion-item>\n\n      <ion-item class="contact-info" [hidden]="!contato.Telefone">\n\n        <ion-icon item-start name="call" (click)="ligar(contato)" color="orange_80"></ion-icon>\n\n        <a href="#" class="ion_link" (click)="ligar(contato)">{{contato.Telefone}}</a>\n\n      </ion-item>\n\n      <ion-item class="contact-info" [hidden]="!contato.Celular">\n\n        <ion-icon item-start name="phone-portrait" (click)="whatsapp(contato)" color="orange_80"></ion-icon>\n\n        <a href="#" class="ion_link" (click)="whatsapp(contato)">{{contato.Celular}}</a>\n\n      </ion-item>\n\n      <ion-item class="contact-info" [hidden]="!contato.Localizacao">\n\n        <ion-icon item-start name="pin" color="orange_80"></ion-icon>\n\n        <a href="#" class="ion_link" [hidden]="!contato.Localizacao">{{contato.Localizacao}}</a>\n\n      </ion-item>\n\n      <ion-item class="contact-info" [hidden]="!contato.Aniversario">\n\n        <ion-icon item-start name="calendar" color="orange_80"></ion-icon>\n\n        <a href="#" class="ion_link">{{contato.Aniversario}}</a>\n\n      </ion-item>\n\n    </ion-list>\n\n\n\n    <div class="mesma-gerencia" [hidden]="contato.Interesse == \'\'">\n\n      Interesses do Contato\n\n    </div>\n\n    <ion-list [hidden]="contato.Interesse == \'\'">\n\n      <ion-item *ngFor="let i of Interesses">\n\n        <ion-label class="list-item-cargo-1">{{i}}</ion-label>\n\n      </ion-item>\n\n    </ion-list>\n\n\n\n    <div class="mesma-gerencia" [hidden]="contatosMesmaGerencia.length == 0">\n\n      Contatos da Mesma Gerência\n\n    </div>\n\n    <ion-list [hidden]="contatosMesmaGerencia.length == 0">\n\n      <ion-item-sliding *ngFor="let contato of contatosMesmaGerencia">\n\n        <button ion-item (click)="showContato(contato)">\n\n          <ion-avatar item-left [hidden]="contato.Foto == null">\n\n            <figure>\n\n              <img src="{{contato.Foto}}">\n\n            </figure>\n\n          </ion-avatar>\n\n          <ion-avatar item-left [hidden]="contato.Foto != null">\n\n            <ion-icon name="contact" class="avatarGrande"></ion-icon>\n\n          </ion-avatar>\n\n          <h2 class="list-item-nome-1">{{contato.Nome}}</h2>\n\n          <p class="list-item-cargo-1">\n\n            {{contato.Cargo}}\n\n          </p>\n\n          <p class="list-item-nome">{{contato.Telefone}}</p>\n\n        </button>\n\n        <ion-item-options side="left">\n\n          <button ion-button (click)="enviarEmail(contato)">\n\n            <ion-icon name="mail"></ion-icon>E-mail\n\n          </button>\n\n        </ion-item-options>\n\n        <ion-item-options side="right">\n\n          <button ion-button (click)="ligar(contato)">\n\n            <ion-icon name="call"></ion-icon>Ligar\n\n          </button>\n\n        </ion-item-options>\n\n      </ion-item-sliding>\n\n    </ion-list>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\brq\TFS_5025\src\pages\contato\contato.html"*/,
            changeDetection: __WEBPACK_IMPORTED_MODULE_1__angular_core__["i" /* ChangeDetectionStrategy */].Default
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["y" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["u" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["r" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__services_contatos_service__["a" /* ContatoService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_core__["j" /* ChangeDetectorRef */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["q" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_6__ons_ons_mobile_login__["f" /* NetWorkService */],
            __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_analytics__["a" /* AnalyticsService */]])
    ], ContatoPage);
    return ContatoPage;
    var ContatoPage_1;
}());

//# sourceMappingURL=contato.js.map

/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalImageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular_navigation_nav_params__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_contatos_service__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environment_config__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ModalImageComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var ModalImageComponent = /** @class */ (function () {
    function ModalImageComponent(Params, contatoSrv, platform, 
        // private navbarCtrl: NavController,
        viewCtrl) {
        var _this = this;
        this.Params = Params;
        this.contatoSrv = contatoSrv;
        this.platform = platform;
        this.viewCtrl = viewCtrl;
        this.loading = false;
        this.notConnected = false;
        this.id = this.Params.get('id');
        this.notConnected = Params.get('notConnected') || false;
        this.platform.registerBackButtonAction(function () {
            _this.closeModal();
        }, 1);
        this.getImagemDoPerfil();
    }
    ModalImageComponent.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.navBar.backButtonClick = function (e) {
            _this.viewCtrl.dismiss();
        };
    };
    ModalImageComponent.prototype.getImagemDoPerfil = function () {
        var _this = this;
        this.loading = true;
        if (this.notConnected) {
            this.url_src = __WEBPACK_IMPORTED_MODULE_4__environment_config__["a" /* Config */].imgNotConnected;
            this.loading = false;
        }
        else {
            this.contatoSrv.getImagemPerfil(this.id).then(function (result) {
                _this.loading = false;
                if (result == [] || result == null || result.length == 0 || result == "[]") {
                    _this.url_src = __WEBPACK_IMPORTED_MODULE_4__environment_config__["a" /* Config */].imgNotFound;
                }
                else {
                    var obj = JSON.parse(result);
                    _this.url_src = obj[0].LargeImage;
                }
            }).catch(function (error) {
                console.log(error);
                _this.loading = false;
            });
        }
    };
    ModalImageComponent.prototype.closeModal = function () {
        this.viewCtrl.dismiss();
    };
    ModalImageComponent.prototype.closeModalContent = function () {
        this.viewCtrl.dismiss();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["s" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["s" /* Navbar */])
    ], ModalImageComponent.prototype, "navBar", void 0);
    ModalImageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'modal-image',template:/*ion-inline-start:"C:\brq\TFS_5025\src\components\modal-image\modal-image.html"*/'<!-- Generated template for the ModalImageComponent component -->\n\n<ion-content id="container">\n\n  <ion-navbar>\n\n    <ion-title></ion-title>\n\n    <ion-buttons end>\n\n    <button class="btn-fechar" ion-button (click)="closeModal()"><ion-icon name="close"></ion-icon></button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n  <ion-grid  class="no-padding-top">\n\n    <ion-row text-center align-items-center>\n\n      <ion-col class="no-padding-top">\n\n          <ion-spinner *ngIf="loading" name="bubbles"></ion-spinner>\n\n          <img src="{{url_src}}" imageViewer>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n\n\n\n\n\n'/*ion-inline-end:"C:\brq\TFS_5025\src\components\modal-image\modal-image.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular_navigation_nav_params__["a" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__services_contatos_service__["a" /* ContatoService */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["u" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["y" /* ViewController */]])
    ], ModalImageComponent);
    return ModalImageComponent;
}());

//# sourceMappingURL=modal-image.js.map

/***/ }),

/***/ 391:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PushService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ons_ons_mobile_login__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_app_version__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_device__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_firebase__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_operators__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__environment_config__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









var PushService = /** @class */ (function () {
    function PushService(httpClient, appVersion, alertCtrl, device, platform, fcmSrv, tokenSrv) {
        var _this = this;
        this.httpClient = httpClient;
        this.appVersion = appVersion;
        this.alertCtrl = alertCtrl;
        this.device = device;
        this.platform = platform;
        this.fcmSrv = fcmSrv;
        this.tokenSrv = tokenSrv;
        this._appV = '';
        if (platform.is('Cordova')) {
            this.appVersion.getVersionNumber().then(function (x) {
                _this.appV = x;
            });
            this.ativarPushListener();
        }
        else {
            this.alertCtrl.create({
                title: __WEBPACK_IMPORTED_MODULE_8__environment_config__["a" /* Config */].MensagemGenerica.PUSHBROWSERERRORTITLE,
                subTitle: __WEBPACK_IMPORTED_MODULE_8__environment_config__["a" /* Config */].MensagemGenerica.PUSHBROWSERERROR,
                buttons: ['OK']
            }).present();
        }
    }
    Object.defineProperty(PushService.prototype, "appV", {
        get: function () {
            return this._appV;
        },
        set: function (data) {
            this._appV = data;
        },
        enumerable: true,
        configurable: true
    });
    PushService.prototype.registrarFirebase = function () {
        return __awaiter(this, void 0, void 0, function () {
            var headers, options, body, _a, _b, _c, _d;
            return __generator(this, function (_e) {
                switch (_e.label) {
                    case 0:
                        headers = new __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["d" /* HttpHeaders */]({
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'Authorization': 'Bearer ' + this.tokenSrv.getToken().access_token
                        });
                        options = { headers: headers };
                        _b = (_a = JSON).stringify;
                        _c = {
                            "appVersao": this.appV
                        };
                        _d = "dispositivoId";
                        return [4 /*yield*/, this.fcmSrv.getToken()];
                    case 1:
                        body = _b.apply(_a, [(_c[_d] = _e.sent(),
                                _c["dispositivoDetalhe"] = {
                                    "so": this.device.platform,
                                    "modelo": this.device.model
                                },
                                _c["topicos"] = [
                                    "Publicações"
                                ],
                                _c)]);
                        this.httpClient.post(__WEBPACK_IMPORTED_MODULE_8__environment_config__["a" /* Config */].FIREBASE_URL, body, options)
                            .subscribe(function (data) {
                            console.log('Registro funcinou e me retornou isso:', data);
                            return data;
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    PushService.prototype.ativarPushListener = function () {
        var _this = this;
        this.fcmSrv.onNotificationOpen().pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["tap"])(function (msg) {
            _this.showPushAlert(msg.TipoMSG);
        })).subscribe(function (data) {
            console.log('o q estou recebendo de fato?', data);
        });
    };
    ;
    PushService.prototype.showPushAlert = function (msgType) {
        switch (msgType) {
            case __WEBPACK_IMPORTED_MODULE_8__environment_config__["a" /* Config */].ANIVERSARIANTE:
                var aniversario = this.alertCtrl.create({
                    title: 'FELIZ ANIVERSARIO!',
                    subTitle: 'O Ons lhe deseja um feliz aniversario e um excelente dia'
                });
                aniversario.present();
                break;
            case __WEBPACK_IMPORTED_MODULE_8__environment_config__["a" /* Config */].COORPORATIVA:
                var corporativa = this.alertCtrl.create({
                    title: 'Items de reunião',
                    subTitle: 'body da mensagem corporativa'
                });
                corporativa.present();
                break;
            default:
                break;
        }
    };
    PushService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_app_version__["a" /* AppVersion */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_device__["a" /* Device */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["u" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_firebase__["a" /* Firebase */],
            __WEBPACK_IMPORTED_MODULE_0__ons_ons_mobile_login__["i" /* TokenService */]])
    ], PushService);
    return PushService;
}());

//# sourceMappingURL=push.service.js.map

/***/ }),

/***/ 393:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(398);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 398:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export jwtOptionsFactory */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_img_viewer__ = __webpack_require__(439);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(458);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_contatos_contatos__ = __webpack_require__(370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_contato_contato__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_filtro_filtro__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_firebase__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_modal_image_modal_image__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ons_ons_mobile_login__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ons_ons_mobile_analytics__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_common_http__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_contatos_service__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_push_service__ = __webpack_require__(391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__environment_config__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__auth0_angular_jwt__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_device__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_app_version__ = __webpack_require__(178);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






















// const config = Config.FIREBASECONFIG;
function jwtOptionsFactory(tokenSrv) {
    return {
        tokenGetter: function () {
            var tk = tokenSrv.getToken();
            console.log("Token: ", tk);
            return tk.access_token;
        },
        whitelistedDomains: __WEBPACK_IMPORTED_MODULE_18__environment_config__["a" /* Config */].WHITELISTEDDOMAINS
    };
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_contatos_contatos__["a" /* ContatosPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_contato_contato__["a" /* ContatoPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_filtro_filtro__["a" /* FiltroPage */],
                __WEBPACK_IMPORTED_MODULE_12__components_modal_image_modal_image__["a" /* ModalImageComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_14__ons_ons_mobile_analytics__["b" /* OnsAnalyticsModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_13__ons_ons_mobile_login__["g" /* OnsPackage */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_15__angular_common_http__["c" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {
                    backButtonText: 'Voltar'
                }, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_19__auth0_angular_jwt__["c" /* JwtModule */].forRoot({
                    jwtOptionsProvider: {
                        provide: __WEBPACK_IMPORTED_MODULE_19__auth0_angular_jwt__["a" /* JWT_OPTIONS */],
                        useFactory: (jwtOptionsFactory),
                        deps: [__WEBPACK_IMPORTED_MODULE_13__ons_ons_mobile_login__["e" /* LoginService */]]
                    }
                }),
                __WEBPACK_IMPORTED_MODULE_5_ionic_img_viewer__["a" /* IonicImageViewerModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_contatos_contatos__["a" /* ContatosPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_contato_contato__["a" /* ContatoPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_filtro_filtro__["a" /* FiltroPage */],
                __WEBPACK_IMPORTED_MODULE_13__ons_ons_mobile_login__["d" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_12__components_modal_image_modal_image__["a" /* ModalImageComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_16__services_contatos_service__["a" /* ContatoService */],
                __WEBPACK_IMPORTED_MODULE_13__ons_ons_mobile_login__["k" /* UtilService */],
                //AnalyticsPackage
                __WEBPACK_IMPORTED_MODULE_14__ons_ons_mobile_analytics__["a" /* AnalyticsService */],
                __WEBPACK_IMPORTED_MODULE_13__ons_ons_mobile_login__["k" /* UtilService */],
                __WEBPACK_IMPORTED_MODULE_13__ons_ons_mobile_login__["e" /* LoginService */],
                __WEBPACK_IMPORTED_MODULE_13__ons_ons_mobile_login__["b" /* ErrorService */],
                __WEBPACK_IMPORTED_MODULE_13__ons_ons_mobile_login__["j" /* UserService */],
                __WEBPACK_IMPORTED_MODULE_13__ons_ons_mobile_login__["h" /* StorageService */],
                __WEBPACK_IMPORTED_MODULE_13__ons_ons_mobile_login__["c" /* ImageService */],
                // SecurityService,
                __WEBPACK_IMPORTED_MODULE_13__ons_ons_mobile_login__["f" /* NetWorkService */],
                // Firebase
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_device__["a" /* Device */],
                __WEBPACK_IMPORTED_MODULE_17__services_push_service__["a" /* PushService */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_app_version__["a" /* AppVersion */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_firebase__["a" /* Firebase */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 458:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_contatos_contatos__ = __webpack_require__(370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_push_service__ = __webpack_require__(391);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/* import { ContatoService } from '../services/contatos.service'; */

var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, loginSrv, splashScreen, imageSrv, envSrv, utilSrv, errorSrv, networkSrv, 
        /* private contatoSrv: ContatoService, */
        pushSrv, app) {
        var _this = this;
        this.loginSrv = loginSrv;
        this.imageSrv = imageSrv;
        this.envSrv = envSrv;
        this.utilSrv = utilSrv;
        this.errorSrv = errorSrv;
        this.networkSrv = networkSrv;
        this.pushSrv = pushSrv;
        this.app = app;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            statusBar.styleDefault();
            splashScreen.hide();
            imageSrv.SetDefault();
            loginSrv.setAplicationName('Mobile.Contatos');
            envSrv.setEnv('DSV');
            _this.rootPage = __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["d" /* LoginPage */];
            _this.loginSrv.onConectedChange
                .subscribe(function (u) {
                console.log(u);
                if (u !== undefined) {
                    if (u.Connected) {
                        _this.pushSrv.registrarFirebase();
                        _this.app.getActiveNav().setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_contatos_contatos__["a" /* ContatosPage */]);
                    }
                }
            });
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\brq\TFS_5025\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>'/*ion-inline-end:"C:\brq\TFS_5025\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["e" /* LoginService */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["c" /* ImageService */],
            __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["a" /* EnvironmentService */],
            __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["k" /* UtilService */],
            __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["b" /* ErrorService */],
            __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["f" /* NetWorkService */],
            __WEBPACK_IMPORTED_MODULE_7__services_push_service__["a" /* PushService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Config; });
var Config = {
    REFRESH_INTERVAL: 24 * 60 * 60 * 1000,
    CLIENT_ID: "Mobile.Contatos",
    GRANT_TYPE: "password",
    delimiterInteresses: ';',
    INTRANET_URL: "https://appapitst.ons.org.br/API.Mobile.Contatos/api/contatos",
    FIREBASE_URL: "http://10.2.1.127:92/api/pushnotifications/registrar",
    ANIVERSARIANTE: "ANIVERSARIANTE",
    COORPORATIVA: "COORPORATIVA",
    FIREBASECONFIG: {
        apiKey: "AIzaSyBwAJ22D_iKpPQTqpbIQMJZ9ZJIcM0cQu0",
        authDomain: "brq-push-test.firebaseapp.com",
        databaseURL: "https://brq-push-test.firebaseio.com",
        projectId: "brq-push-test",
        storageBucket: "brq-push-test.appspot.com",
        messagingSenderId: "911746189843"
    },
    WHITELISTEDDOMAINS: ["localhost", "localhost:51078", "appapitst.ons.org.br"],
    MensagemGenerica: {
        MENSAGEM_ALERTA_CONTATOS_SHAREPOINT: "Houve um erro no processo de carga dos dados, \na informação já foi repassada ao setor de suporte",
        PUSHBROWSERERROR: "As mensagens de push não estarão disponiveis no browser. Somente nos devices",
        PUSHBROWSERERRORTITLE: "Problemas de notificações"
    },
    imgNotConnected: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABQAAAALQCAMAAAGPpB3yAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAABmUExURf////+3t/9/f/+np//v7/8wMP8AAP8ICP9ISP/X1//Pz/9oaP9AQP+Xl/9QUP+fn/9gYP+/v/+Hh/8QEP8YGP+vr//n5//Hx//39/84OP9YWP/f3/94eP8oKP8gIP9wcP+PjwAAABScTswAAAAidFJOU////////////////////////////////////////////wAN0MNxAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAluUlEQVR4Xu3cUVOjOhQAYB2109HxRd+04///mTc5CRAgtMXVde/u9927bUhDCIdDoFV7AwAAAAAAwG9wqM8d7UvP9XnT65mOGo/1+by21WFziKf6HC4O8HDdAC95r8+TK/u9OMCQOrtNDy+59FG6Ptw838drZbE8l2IUjvkxopT+f50GWCrSC6lJXr6/ObyNlen/NKBYK3V+l58vDbCsl1u9xVJamP1rpMVSk1qWrafF0v9hMcCnKKQmeR/HbkrhWJ6HDLg4wKSW6lPq/Gka6M3hNj9mabG0iUyrAxyybj7A3G7ch1gp591HLqRoR91zqb84wOxU2xalnB/zv2b9tJj+TyOJFtMAc+gnpVUUcpP0lBbL0Ep9GWBkU3ZhgNHocHOXw5hi+Z4eUmbUpdKiPCW5sizn16YBvtT281YpR4/5pVzOR2Xorwyw/MtVQyR/0I8P4JI/foAAAAAAAAAAAAAAAAD8C675Y6rk8l/DXPn3MkOzrealvnn1dKltsdWmEX+7dbVrNhpmfx+57eIAy9/APZTnw81rqb0vS8n0fKxN8r+xkMXfdZVi0yr+j9r0cH/zXrqMts83d/XF8hdiZ73kh2iVHp5KKf85Yf7bwqq+WiKdFvJy/fvKvFAKw+PYKv5wrjaqpXhM/09/qTa12PJ2yH/cFq1q0/zUrPU6/qXjbIBjm2ETw+N8gPlvHMtyfp4GGH/VGYbnLeN6U9P8R5BRFUFMz/XVnQMs/1dNq/R/OsHzHw3HXzRObfrqn2TG3whGLNO/57yQSvFnmxHh/Fw3HduI1ovFWJ5aHeJPLMvK9SG3iv/TAHNN/EVjqQEAAAAAAAAAAAAAAAAAAAAAAADYoXxd2hd5/MLvRNvo6yVXl29t2+FwWwsLhyu/VXLb8B1232Rn5++X25++dLy/HMDD23SkD0l8m+Qxl8aqKM1r8rc5Fu+xXBeSXMxfKFkq48Vx/SiUF3PD/DWM8W2D5dWP+jz1lgtTX7kiOUUAU3VdjucolVDc52Jp36yaXyyl8pRfyBv85QBG8MpGHuoGhi3lWObvMywL9TnOn7ZtMS3mUtm7OtIo1tWaF+tG43Hc30UG5vK6r2gd1WU5KbNQ/vrHYUKKl+p3Od7lhxSnj1KZd6usGe1/MYC1p/L9llU+OiHK5WtD74ZtDt89WZ6yU21a5FJvp6smgPFYD2CRiucCWLcSByOqh0HVvvLT81DMz6UcJ1XEKef7bbxQDS983jC1Rlf5SGXDsAbDYq4/xNezTnXDIOMxy6VOAKdvb+0FMIrhXAAjw9oAZk2k8tOwfjeAuSLqaqPs1wI4hipOyciu1/xVwqU6TXTDF6LGzFgGNX3BbYjS2E9ZngWwlscvAO4E8L6U42DWV0Muz/qq02R6zNXNoMrLeWauxfKYH9oA1vp2gyU1y/K/61/f/18mgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwv3Woz/scNlb7XG+trZ4/5/BaC19go68Y795B32+t8McF8KMWBjt7b5uv+iqiyd5Bb7bf29HK3eFrI7jwvrPzK5p/7XB/ubfD/RePaO4fCGD6/70Un1Iylv5yoZw/Q1V+jsL9WCpe8+JjXUjtnvPDzVuqfMulkArv+TmaHXLlzc0xnmtHx1rMT1Pni77qtssul4dYjoVaGirvo/ReVohy+r9MqU+5YthgvPBLHlIHH6WTtEv5jE4Ph7v8kLcXr6TRvOZCDuljjG0Y7M3N20N6iJWKstPDbo0Z+BhPZd3oK+9CXij1+fGlFPND1fZVI3Jz81xXSf9i3HlQh+doU17Ij89lS9FliVsq3cbrsXMbG/yMppOxqzKSWB7qhiS9qck2ti3Gl4edjmJ+rAEsq73lhdp7HK60ND40R6xa9lWKpcdYHgcVVVFXohJ5sW6fC21pucHPiPXLGTJ2VQv5qRz59Dy8OLw2Ng6HUy3UnY5ipMUQwHgsXZZiE8CIaz236quhnAhRLCkWxfIUy7WibDQ9pbPh8BTF8nqtHh6ifU7brQ1+QhlDHLbpPMzzQ5aK6Ro91MXIhkbj2FOI8izYC2CctiWAj6XDWKuu2QSwvta+GpZ91dfiqTzUcA2RSpVDVkb7RQBLZf43GF74vNpR7qUmfzLrM11ZSiFGOzSq02ZyOLykh4sBzI9FLc4CGMXQFNsM7AcwPcWW2wBGqTyfCWBeKpriJwz357nHerIliz7HzaXndoVSKFP0hQC2XdbiNQFc9FVfi6exXbxQI3VM/9LhjHL+twxgOuyxra0N7jfc67fjS4Vy0zAaXsinRy1PbcvlcTOAtTy2H4vri0iYLkepvMzAesKOD1kMqixFm1Is09sygOlxeB41xU8Y186Fsu30UNMsDb+kV1ocrzK1UWmRROn1YgDLrctNnG9RmgewlOPWo76cLfuq9x3lMf1rBlUeYzlSsBTXARxuZcpTbDD9KyP9hNO4Yh1Bku83XqKUqtJNVzyXl3JClFKzwbx0NoCldX5KDXMp184DWF8tyTx1vszActNeavJDXipt8vvRYb0ollv2VQDvhoFGo3KzmP7Vq/K/q0aKzxLAX1QmFz5NBgIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPxhXg6HWvp+Dzu3dTjc1dKPeT8ktfynOf2hQ9s3rtS6ltJq77X4PQ6Hh1q60uHwXEs/JsXkcDjVhf+bNPRa+hZf0/3vmwH3b+jnEzCfM8+/LUJfTQL+oh+/BD9FcDpXlLt0N/HxUheK51w1nysfj2nVp/u6lKwqkqf1eq28pcPxti7NnU61q8dTnKn52vcUFcnj6ZSGfwq16ubm/i01eXisS0lZM51jr9FDr6PBfAe73VdjzZnu7lMoXnO7KQFTl7WUyvNIlVfu0wgeosfWKY9r1n1p/Z66aHd1CNdxiFpxOtZC5xD+9Ax4X2OTQhXP1UeKzfF0yiEaYpeLT7d3+fCOyZqKb8+3L6+HQ02fVcXNbW7/VrtaJ9ljrj6+3L2lVXq3R2N80vq594/nxzyCEtHT8ZjieUyPxxrilGaH15cYwBjYYc383Cy2HYW0NN/BdfeTs+MKuZuX25TB6TSuHZbWRapfhi5i9Xb3koM/ZdtdWhoDOCZbClZunXY1V9fKcVhtVbOUm64P4c8m4HD3lxKxGUgKWi2lcpxM6QwfsyftQAnE6tZxfS/Z7GokWy32TAen0R7o8RxJgx2O9Pwa2W5gLMeRK8Vso6O0g+PRTTtYz4XNS/ClcaXqIV6pj7GXVF0K3UiNI0gdDWtMpVkAc+vhfG2aDMNq1xq22T2E4xo/JJ2AtZSGVEvz4Rdp15tJPc0L8ZzOwHgedSpmPc2miJVeLNoDXQrZNNhZhsy3NgwmrdmOqt9RfwevS8BSyNrumtk+HfpaGluvIpVqmpRM65fXtwI4az1NmMOwUgYP/addiEzd2MOfTcD27nja1TS3z24hknkchvcs+USdxXFZkXZ6tnuLcC6sZ4UmPrMDnUZYS22GpK23N9TDxmZrbnXU38G9CbjRXScBV6Gbegy1g80AznZ1umCNzcutfTbk7MYeLjbwmy12o36ENMVrlEY/V+vzFJouQU2+ziqWPS1yIeTbtsGvJWDbUyjzwmKjmxkzV6q/LQGTRejmmVA7WL5/Htefte4k4NDBdI3LW5sZqtvN/mZ1JJNy2RjPjkl6rZY68oGfvZcfK1KhuRL1ukkRHT85/dUETOX5pFLM1tzqaGMHvzMBsyZ080yoHaR3IP0AXpoBczHPfKmDsjitOveTCZjebdVS0Zwsb6UwWp6JC9NuVkPFdIMeVh2nAzzt/68m4HJr1VUJuLGDn03AlHLNVWE7AZvQzTJhvJvYCuCsdTcBUxfp3m96Y7Oxh7OOfq8UrfF9XzGcJOkGYppKnuMWduP8qdKtSi1VQ0UKb/O2Y91JajduKd0X70/AWQBTzDs/iLoqAbd2cOv4XBpX210qDwvrBBxD17RqVt8K4OUZMG4D24C0/U+2dvA3WB+tdADLCZYCeTjmUzi9ratZmkvlU9PbNHNGpqSaaJ6CVBqtKpJUjFjlT2Xnl5OQPyfLGZ6vRZ9KwDRt522Obw6HI5NmndpmcdC3Opp28HHYwWTe/eTiuFJvr3nH8m5P8+jQOlUuI5V6TAt5uzn60+eAaaETwFne9BMw1mwG19/DcY38ain9Jr3tTe+dInBJe1OVkyWbPnuvjaZEXlUkOchZ7/4sKamXD8Nrp8l4pqf4lEI2uzKWm/m6cPMYi8mYzLM1z3U07WB7osy7H1wxrrJj8S5jrJ5aryIVPebTZkqnqhfAcfvZ1M2sOi21e5et93Bco3/54J8xm7zgd5OA/CgJyI96eIiPGwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOg5HJ5q6fu9HA6PtXiNt8Ohlvh7/c4EPByOtXSV4x+QgHeHP/cs+FOHtm9cYwI+Hg6vpfRdXncG7E9IwBTMw30t/2nS0N5r8Y+yb1xjAp72Je5+KcNr6Up/SgI+1PKf5uIBe9h3xdlrq/t9idTOgB+l9E0Oh7tautIfkIBP6bLwB5wGn/TNU8rXdP/b7gGPu/P7D0jAw+H0vu+d059EAv6an0/A+zyCbz6M30gC/pqfT8CPPII/4Vb0cyTgr/n5I384PMfjS1mcpHvDw3F2Zb5/S1UP84t1fl/3kXsYrCpubm4f8nq3dWnl/Sndg663Hx5PQ/3plB/fU8CmEZxOeWsv6ek0vSd9Tlv7iMZVWTOt+NYszjuqFjvY7b54LL0kZ7p7Tif3Q/58YUrAcbXkJXX+Mb5neDxFgF7SOqv3PSWAbfePpwhxHl+b2zVcL0+zaN5Ni6tD+OMJeFsGkIYVi6NUUYwRSjeK1ZRKOUnDkHGriqaqPw1ML/fun1Pkayl/7pFuF4ryqdG4mEVNBLSYestrPkddfAY36ygaVNMOlgM0LSdRM5mOW2dcRU6OkNsMu74dqeixWWcyRWjK3midkykrJ1ZWhxVXtcn44WPnEE478kPSYPJT2vNYrB7zIF9e8jk6xC4fwtfT7UuerYYg5gicHu9yhDYqYgOHj5eXCG3nQ9hc/ZC6TYHovTxLwHSyHF5uY1Cl7viQV3t4OB4f6pDya0+3d3FoSk2smdd5+ChJ2esoix1MtdMOrrsfTcdtq7uUBIenu1N+Svm5SsBVpHKPaWHdUV4aAjhW19Yfz4+5o3HKrMNKG5ySMv+goUzgvUM47cgPGU6G2ZDzvtWLznAy5ZSMQrxYz/Rx12/ra6uK3HjoOBXXk9x0MqawLK9h8wRMB6aELQVyzNVpWFlaqpFNjYc2qd8p4JsddXdw0f2kTcC2u3EHp+2nLjoJuIpU6jEp5ZxUpRSrdwLYth4L07Caqmmhu4c/nYDjzDcbcsqFWsrnTzy1rw/lFPB4Hq0q8nGYrhtpvVWKNRVNNozaBBxPinYwTTH2ZuwvHcTaPK/ZfDybFy90NJXb2labgJe6i7mwloeRrCMVKVXL+QDUBNkIYG49RCvNd8MUOAwrBWK6GxgyuOl/Kv90Ao67Mc7TWTOpVykOU6qkYxvPq8Gv96bd6eXSUu/VWQKOI0j3MsO8OVtpsTBNQe0PevoddXdw0WNj2tN+d7MMSOm0nAHXkUo1s8HXuW4+gHFp1noqj902RzDt2fDc2cP1QH6v9uxtLx+1NFrGId5WTcepWlWku6MhU7J5L0tTsk3aBGzeQLZHdFopXWOmoz6tuthqv6PuDq7WHU3H7brulgm4itQiE4b1twI4az111iTg+PpQXA6p7OEPJ2Cz+eay216Bq/mcWJM1nfDzmXJVsdi92TS7km7Ia2nSJmC9e8qm7bRhnR/VsbvFz0f7HS3vgbsT0GTasa3u5pPuMgFXkVqEatiVrQDO6qfr+VjdRHrY5HyLwx4uNvC7NXFK08dwnNZRT7cZ7UEcGqS7m/lt27Ji0VMKezNdLF1KwNl93DBdt1uYb208LLMUWSwOHW3s4KLLyXTc+uOa72ibgLX1KnTzTBgu4VsBnLVO02QttcOq3Q93+Rt7+LMJmPanvVddDX6U30DNlfo0V7Z3FquKsWE1n2cG+aPjolZMNmfAYYTtSqWPxlC9SMBZxpSOtnZwLCy0R7ozrhTY+ZVzOQOuQzfPhGEGW25/COCsdRp8LU3Vw6cX45A29vBnE3B2rW2P9eLykGeTuSGg5YXmc/d5xTKV51emIr9JHNSqyWYCbsyAc0P1IgE7HW3tYCqVwsKFBExHu73X6CXgMnTzTBgyeCuAs9a9GXCc8IYs39jDn03AOpTRGPXh8A5SQNt5rlXOrObmv61IhXgerGfAaP0Sne+6BxyOS7uF5daq8wk4Zkx3Bze63E7AErlFd90EXIRungkpp35tBhzWHO+LN/bwRxNwdVLUsYyFUTohm1luKV9NarEYKxYvpNNyeQ/YtPjie8DR/OYnLXYyZmsHN7qcJWDnip66a5O+TcDZWJrQzTNh+CBisf0xgLPWvRlwiOZ4NdvYwx9NwMXejSdLJ+pTCHvSmvPEGioWPa3fBaeGY81vmwE7GZMK3R3c6HKWgL1xzQOyMQNmY+jmmTAsbQVw1ro7A6ZVc8JNFf09nG/2N1sOaThbOoOajnjX6jjVikVPUzoN2hXHE6Cx6x6w8/lltkrAXkcbO7jasWrasa3u2vXOJODYch6qfu0Ujll9dwYsXTQV/T1cbOC3Ws04w16nHdoK04bVy7UinZvzD5+WvbQ1uxJwCOasy/6b7MXe9DtaDy1sVDfH7YruUqCvSsDZzFzebGwFcJY3/RkwzZZ5hfG629+Vn0zA1YjGH9bMXolIpJuP9fvXyaqroWL2QtrZ5X1w83raxKKT5OIMOHsj3+thseZWRxs7OOu+MR23fgKm2+vxGpxvtacEnN8D5prSU+pxik67280ApgDO8qY/A6ZVn9KNX13Y2sPZGr9XGtDyrnSIXzplh2ENZ1c6Es1dTSmOt28pxOVDg2VFjs24g6cpbUYp0vX4pQ11YnFxBpz90HWew7f1hcVB798DdncwItG8wZ9Mx62fz6k0rJgm9vUMuI5U6nE8IGnyGoa1EcBZ3vRnwLxHs/Onu4ftjvR39dtMx3Y07mwORo5UikRbVY5cikPUpdOrBC/11K9I8qd8cV3M88BQ2agv36fopBalrrGZgGMql17vyrU3H+ynCONj2nCN8WzNsx0tdjCbdT/ZTMAhcXIs8vbzeZX+LRKwE6nUYx59HnwqjtvfCOAs0zZmwLyRmt1FXl7uYbsjbX7+BuMYJmkfazTzWIshok3VUFkXslheVyR5Nq1W818SUQq3afVaOdlMwHFY+RBmZSndRUzqCT1bc7ujVJ5sdD9qj1u/uzh3Q65ezYCTWC495rwoamXWDeAs0zZmwNhILRbRSVUHOkvA3uc032dxZQrTPtb4fTSz8mOapcIQzTHI4yFYVWRpH7P464iO8pOQfJIvwpVNb0wWV87pdiZmhun2aThe09+gLHZ0s6NpB9uZYNF9dcW4au7mvtoErK1XkSqZUIKxSIROAKftJ+kkrqVZdQ7FYuZe72FqU0vppVriXzSfvOA3k4D8KAnIj5KA/CgJyI+SgPwoCciPujsufw0dAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPhn3J5e7u5r+a9yejgcju914as9Pj0cT7UM/F+dDslbXfiLvOf9OnzbHJUm18PhuS78w9Ll83R6rAtc6yVlz+ttXeCib4xXbwKMzd3Vhf+n57xbr993Z2sCDBGGv/IC+q0iOw+H73p78tf5znh1JsC/4PC85R34zreoceb/v68RXyCSJ3Mp2OWuRO2v/OTpO3xnvPLd3mIC/N8fnvuPNPxvvP1LTIDJbQ7CMT84l/e5Ox4fnn4lZuUTnrrwv7N79L8cr229t8DfuLnfIcL7zT+hMAEmOQYf5RL6Wqv4Pf6xCfAb/YU/BHk9HB6+e/o2Ad7cxL1fCnTE4qlW8luYAL/KXzgB/g5x0v/bn3xF5uQQ3OeCD0R/KxPgVzEBfooJ8DFH4BjF8qHx//czk/8hE+BXMQF+irfArzkCtRw/dP+oC33vT7nN6PWl1s/dRVxHG61ubufN3ta/sHC5RXYbA2887fnVh/uX/MO2xvW/OhafHsyvn1FVNn9XfrBUfMx//a3cbq+tor+IQDeUw4cYqXUczjBOBs2Ibs+MaO7CZmv10vncuRSvq0c3H1xJit4E2NlcciGnYqVSnOfV25lbg8d26Fk/A/ft7Gz4JWG2fiTSvXe55iyMJv/yBBiHeAxAnD7bP3d6X8wUxfJjw/rrVwurPIyf8S/NDuHlFln9ZYel124GrvQHezhc90vhkaLz9ImMSttupqJBuc8urpsArwzlsM2YBAZjT+dG1D+nL2+2Vi1dmgA7p9uV8Zq8rxseHvoTYGdzl3MqVkrPt52W/U/I47co1joZuO9QzIdfDko/L0s2zSa4/jGcByPphOifEmGaDmu8Hz5sXHvv61F7GQ/VXa1pj3Q9CR+mI/VYM2ke5jJvHadtxf1Se3wvt0jqcX6dBjXdeF0xiZXRfjRr39e10zl1WbSdz0clx8uwHoax39dbiVWmnX0TGT+XvyKUdZvlPCyzxv3LcTwd9o1ox2breVcXrtE53XaObpjAxiP2XCo+4p3JxQnwipyKleqkNt00DbeN67uoPRm4b2cXw49O+7+mET20GXvtWdgJ0T8lzr82piX568JcPWx1aZRjP4W55Ofr8sCXddv0jKl29Tue7fLlFuPpsMrKMq9c8TtUp8PH+uiXy8A1Pw+PnJx3UE+UNPTpLMvKRwfLm7czE+DVoazbjObL1tm+Ee3Y7P7PAGMonQtGtiNei6yY7qd6E2DbxTU5NQ5omVZlVlukxb4M3Lezy+HHbi52MZSDUxeSvanz706AEdJZnCIgvbufcu2cH7fifkqgckvQOUarOSUO+dnAX25Rs6z7Tqkc7GsmsY5y+a8L52xPgOvf3y/nz6L6zBQSr3T2rTM9D+dVf8LfN6Kou26zn5wAfyFeZYProzq8Lb40AV6RU+OA1m9gS07NgrwzAy/sbF0YLKNVDsFymhzOuunUvP4sXG/j3xJ3zvOLV+fThFCOdG/+a8WVp3saln6nuTYC37tfGV1uUebv9ekQyoS9zparRMeXdjaJvJ1vo+R4b90IziKy21PI9aEctrn+eLTYNaI9m/3CCfCXR1fO7d4E2G4uKs7nVB1Qd04obyzbmXFnBl7Y2UW2roZf3p+tDnO0a6Ky5xiutvEviWvU8m6vHLZVksSpvnGkR3GANj4Ijw6mBI1cOvt3J5dbxPV883P3mN0/+ZctnStDV+xU54TuZlREZxHuzSlkRyjrNnu37WHPiHZt9pMTYOeCceXoyk1VXViIdLk0AV7Oqe4YR/ml9izYm4F7drY3kjgEywMdO9VU7k+d7oj+fuVysLqexEFdJklJ9QvXznI93Jg3loclDkX/zUN1sUW8vjlNlfexnzu2kbrda+hcDLFzQnfPn7i0LBJzcwrZE8qz5+y5HF+PaNdmv24CvDJe0XbjKrw5Ac76vpx1Z8K1Pjfy4p4M3LOz3ZHkmkVilnvfupDtT53PnST/e7277iwCuMiRuPZeup0q58Op5+UUudMep5IeqdduQmQXWmzcq47i5Stmscb9bRHnyWZiT6LdPH22czx2Z5HjW1PIvlCez+IdI9q32U9OgJ+PV67Y2s/rJsArsu5cMMst6PgeuHS2IwP37Gx3JGW2m20xTtfmXfVXps5fLWLRuxaWCM4PU6TX4visDMl1Rm1ZvEf0w0P/GJxtEVezM6dfTPCLU6LvZdpM49o7wPnAtjOql+NbU8jlULZXo/NZHK9ed9btPIJfNgFeF6+yuY35ZnMCXPZ9Keu2wzXEZ5xtdmfgjp1NeiOJTbbHPk7j9q543zHcHtHf7mKcZj8FW37O0FWuj/lCs/FfJ9DNb6t/9FN7s0WM6cxdaax3cQK8n7pfuPYOcJ6i2xnVy/GtKWRfKLe3me0Y0c4j+LkJ8NPxKim7/ulsuHoCzM5l3faAhh0eR7A7A3fsbNJtHSk39bl+6/yVqfMXKx8AnrOO6uL4rJQE3R/N+5e4jU+2fsrSbVGO9Mb5kMTLF27j6i93vz7PPwrd9RngfIcjo66730q2ppB9odzeZrbjrNt5BL9sArwuXmVzvZ+hJpsT4FZctrJuO1xDyo25sjsDd+xs0h9Jrpz6iKVZ8n4ida4+4H+R2PGH5row+y+/OMumEtXVD0zmyqR6xbzRE3PJ2T9gW7a4cKTLxyUbp0tVPkZc39nu+inwp0/oZGsK2RfK7W1mO0a08wh+bgKcH7Hdo9s4LLsnwNDJunMrLT5C252BO3Y26bcuG62nYjSZD+ATqfMPToAXbt5LkNvYryp6Ip0uvVHeFGufeUOxapGXNm8a15+WrG3myq/eAXYzqpfjm1PIrlCez+L+eZStR7TvCH7ZBHhlvOKwbPwId3MC3IxLscq6cyvFPWOTF3lxTwbu2dnN1rGn5RjFFpbb/8LU+WtdvDsql7rmjm/146OeckN1/qZrW5mOztwCLluUG9WND8XLyXl+yi7zfGeLsbe/8FPgq3O88xam2BXK81m8Z0Q7j2C0vvC+oBVDmR+TrxpdTI6fmABXWRcr9Y99+aFH03ZvBu7Z2e3WUZ+n4djCMqO+MnX+VlfcJce1ro3tqqKnTJPn5rBzYuVa7lu0iKPXz7+SfRdu4soEuF6//C3ltXeAnRP6yvutJNd1k3VPKM9n8farnRHtO4LR+OrpshucXfGKePdm3DIT7X8LnMSKtZzFSt29KjPLLJKl8dUZuGtnt1vn+rzRaLCOxtelzt8q8uj8ZFYOX3N3XW4a12HN354yHaSSoZfvnR47hyeu4eMWL7dIysfYnZkqWl6ewqLV8t1C/fv233MHGD1039VdG8pk+7zKtl/tjWjHZs+Nvq8TnH3xKgd8OeGUH0YkFybAq3IqVsqWU2CZVxaR2ZeB+w7FZmii8WuZ9Xuv702dRR+Pp+PD6br58/9pdSffUxo1oRl+YNpGq34PZBPqcpVsv4eneD+l1cdDH9Pry2wEZcUxBaLFaXZ1W7QIJSkXX6Y2fHXR5WNYR9sOtt5LJBenz5pp84TezNr+BBiVyxO3GEK53I15KLPt8yrbfrU7ouuOYHFm9F2d4OyMV03CJible55eo/X5t8A5p14v5lSsVDbTfgVqnWRXN4a7MnDfzm63jpMz9nv5AWBx/THsbKPu0WY+/e+Vm7uL14cyt7XHdbzQHh6S8no2D9U0gzw8nE7pYlKSKRsblvfg2evDU2pSF5qeLreopmF8PBwfHurdW1rt8vSXDe0fjqfT0zDUu7g1vOIKGhufp0+MtJs83RN6DOrH0ylWrdWhCeXxaSOUWSeLG9uv9kd09WaTc6Pv6ASnU1X1Rxe12fEtDa0evnSwI6vPT4Dlhj97TZm5lVN1QOUOLoWg2Ux/utmRgcsBTTYnwG5oykvJ+uBVv5A6te+rbiD/lyIUV/yUKMIwb/c4BH702rlPep9SovExi2jnO3EXv5J6uUW1+IrzbPsPnVZW33IdSZ4L194BdtJnz3QzJnNYHJdrQnlum9n+EV252XBu9CvRuDPf7Bpd86ccRay9OQG2m+t8y3P/F6HzSsNXiA62v6T/6gxcDWi0OQFuHNb8UjK775z7dOrUuXPHR7v/M++nl5fuTLJ093Kav00N989vD8fX1+PxafHbw3PvLw/H4+vh+HDc3tptujIdD6/Hh9Pzxv3a5RbV3csxXdI/0qiab+a91uPL28NDulS+3Q3r3p9OG5k3c393elkkyn2K2kbuPJ5eNkJ295R2M92E9lfMoUxX8Ahlv8V6GK3tV7dHlFzebBWjPz6cbxQ6wTkfr/GALLy/vKXbsrTN6Si9v5yW+9Lv+0JOzSap3DYnRuc8WLomA/clx5nW0X77xdHFY9jbRq67aoIA/jbn7rsA/momQOCfZQIE/lmzzwAB/iUmQOCf5S0w8M/q/G4nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPxRbm7+Axdg76efaBg/AAAAAElFTkSuQmCC',
    imgNotFound: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABQAAAALQCAMAAAGPpB3yAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAABmUExURf////+Xl/8AAP8YGP/Hx//Pz//39/+/v//v7/9AQP+fn//f3/9QUP8wMP8ICP9gYP+3t/8QEP94eP8gIP+vr//X1/+np/9ISP9/f/9oaP9wcP8oKP+Hh//n5/+Pj/9YWP84OAAAAHbm07QAAAAidFJOU////////////////////////////////////////////wAN0MNxAAAACXBIWXMAAA7DAAAOwwHHb6hkAAA0T0lEQVR4Xu3dbW+yOhyA8Y3EoDG+0CUmJkv8/t/y/J9aWigKm3PeO9cv58xSSltKKQUf7jcAAAAAAAAAAG54P5+6CK7ypY1+1KhG361g13X7bie5+J/PrnvrDhrqTvpfSmQJT5FKYzzNVf9a0LbWZUnn/+vGH0fbNDaSeP1f81lON7H/3t62FmEkf4vMUZbAYj1orxI6pvCwLH9Tkhwe57OCZ6H/2e5FyF53kURFAnmx/y8pzaetjXhflr+6uNP8fJWmDfXSErqF/vcey4lE1hW0VJq1/u8kNF9Bc9BwmXXks4Knl/8O9qp7Z//Jn3EFrZvpClv2oFXIjqvFFBWMmKP8taxdyueLtEd9fetn6LavXT88Uh+vL2u+M9ZriqXxUPWzXr+CMZDFn/dPDVk4RaelbrvxKKmgrUhrf5aUcY6gsnLFR7zaX73q25JX6d3+XtLl5YdFNezCKf9LwGN06SoLUlNL4+ss6BXUCHn5cVLIzprIF1KZVfFX+b84qF7BvUX+vK7rteTPz7fzx/XzbdO9W6nd2frgSSeB8tpr48nMUVNaH9RZoP6x1QAAAAAAAAAAAAAAAAAAAADwatZ82Dl/EH2l8vP2q/3Ep7HHX4n4bgX7nX9zKz60b58pt69t6UfM9VVD9gVE/Yx5nz7Rb19H9AT2SX7/uqEu6+JR/vgXGQ/d2WJUkc9SOal9c9A+354+5F58QS69etBaRF79y4aRfJ+SdLY+hfU1pbc/8UXGxTwjoTurDWG52h57kZZA/sj69E1D42k04EmyoYKRny5HYvkju13lc09OetJgFOaRkwqaS7x6GjFU0BvHv4foW6YK+pL9WftNE9tOaQVlz862r53WZ1JBKy5F7Q/6zRdf92Gr9havy3v77k7+IqNWsGxBy+ervrHpc3Sn/uXrCIwuZsWXkF+m+75IBWPcO/Xyx74J7uXLq1Tw6EuHzUEqGCl9CD1cZQfs8vYEVgmvmX+Dzv/3r88peU0tWHxjMa39ed3b1i8t2jwxX5H/pYIWbZc3bUFfkHVaW0//HHHsVHkRrlpQjn4Kemxa9+P8O4b6PUT/imLf9RbtX0D0K87hYgvnlDI20u8w2noAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD8Dv3dryf+AkT3rN88mfihfez0l7DsF0seRH+h44ZfbsD+7e1kO/uefu7k7XRIP8VzPpz63n56RCL9N95kaXvYaGhXtpFEn3fSckY2i1AZdNf4uZVqg7dL+hGW0y5+02xY35+6vpeQlL+3H7Up8vy85gY87cqGrLLvq6o+jjWg/WSN/fvwEeE/n6fBTn/E7UNDF19tkZ22nv/yjW9lIgNteWuBrS/rn7cuH5pIpcvFBh780GpooKiXr/d/115i7HWcfWcNONTaxea6qtjgwYZKpL/DgdKIYZWHPGiH1f91/hQvPPRZR3jaIVk6F22dhWyDtDoFrMGK9bkB7cVZEulm+jL0vCGFh0b1eTTL0zOOv9qA3l80Qn9uKY6k/nHeKPbDk2W8h6wLDZuk1ek1WsJ4sNmA1s7F+mj3aMAiez8+1oCdndyeQnnIs88bPFqRcfyVBvSD5r+6qSETdVcLGlBDdsqkfuovYtJZfA/Tj2GmJtLzsVi/ieh40T9F9mfNtKi185Bnr6FnnsLS6+xnIVOkJ8ud8W4Datr4sTXZpc77RqLZ2M9R+nrfcYvNgTgx7a+vj9+69Kgyewnu4qhI0GvtPOSbFxs8VVS4PO+wjh55m7EA+Du+MibahLgtrkcjNzb459GA32S/cxpTN7kHtFmb3u0V04y4nzWS5Jra46MbbuvEXrfxBtwMW5Ub7HKu/tuqf4LvyLA79dMrD6SbuLfelk/WHp09MRg2jKR52SKqDXwKqTE5wz8gdrje7130H5kHK3/6oMtxuyXt8e6rvIU0zgPFKayhYoN4tuAJ8+3Kvy/tVg5GhO1o0Z9M1YAWzMoGHG5l5xqweP3X+X7o+eh3+cN+aWB0rnmrdX5GjjqRrbKfMvagPTmrNrBwakdPta3z/xdJZ0lXDtk5aRTrPUe/xRVyOSmfX8rlQeJ8t6W1youI56M/Ce6pfMgrN3j3lhRXb823U3r0CgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOB3dVf94+EneF5JI10fgQfr+q57u9CAXyY7dJImfJw7ef25Bny4l25A+X972Oji7qR/1W4Xgbfdoe+9mfeHWC2LJwu+H2zZbeX/S87gtNtH6O10yEHzuXu313qDc8S+vV0v/lqs7zuvhPx/tZjPna5Ve90uGnCnI29im396WDY4pA0eyhqwe+s6PeH8r9h03a7zYNcdPiT4YcGLxFtkr+v3b91RXjTCdJLw2nV2JHwzj7bMLGjOlo/uTbnB1lJpQxdl5/VHibE4raS8VtlLbtaARa1NJ1s16vNY3oBWSNRQ/2ikBEd/LNaD+jca2/4Yz+CziNCe5KkORaz+2erfcgNru7ejBKyD2Zpqvf6J+FBkb3+LWpt2fR7MG9D6uVfF9szpug9b1H737issVQT9bz4zPINqD/RPhNP52Ukj6YtsVmxwSlul/dWXMsMUnbIRVfb+Ivw4q3Z9Hswb0I5+dJK6Kldb1AaM0m1Py5TeY9Skwtr60e5D5aPBdZ5YbGBjhErp9KQoM0zR/mI0+/0k/kYDem94rKIBd0V5MpwIW5fiLEalyPS33YApbTp3Y2VaIcNttUHOxQdZjygzjM3jJWdfdtii1qbcPG/waO0G9KCvU2cLaJwbUsrfZgNalnrE02Q6b+6FmWKD1G45nRZeZWjB/JKyzx1c/i9qbZr1ebR2A8aVS/chj3Bl6UNK+dtqQA/aJh4Z46wGhpF8skERKCLtbxGdVlTZ6/+51m6S/RMb0JrNGnAosyh9SCl/5xvQ/vpuDVvny0VK5as8mGd0duZP18dLmb1XtQjON2BEPNRMA8ofGVBsndHuE8v2R/7Pf1sNuLVrgLXCWS8X78XV05Jt9E+xgYf1jxVzHOLK9RGWS0fO/irNtpV5qa6TP1FrU2xSbPBg3h7jBpRqSenDnvjB3WtTWnMVKdsNaGm9lTQ+1gTN3IosN5A+F3NeuxRYVLXeIz1cZt/bCgvmWrty87I+TxVFduUtG1ZIDegvWK3rjn1f3cpiJWm9NEcDAOAB8mQVX+P3LivdmAu1V/3hyRMN+E004DfRgN8kDXiMhyey4O96eshb9mKPSAa26DH6sCU/pRUf+gDKV+kDGQtUG2wlHG/3DqF/XbeX+7uzPaPZ6QN/3/HuM97U08Vd0UyW2J++axP3xc2hNVRsPrx1VW2gT1k9qO96auDfl/bT/ioN5eeVvja9XSHL9hTfYuNd02GVhSI7ZQel2CA/U9Q/FvwT4kwtHl5KKL/lEK95f4vlOAXtbSoLVC+ibCkLxpPIIvovSA3ob5ILa8rUkfRzLCLtbwpYG8Sq/CZc9SL0KFQbxEBg4Wrw/KelBvTdUtYX5Srg0U7DIr2NNCTOq6oGtEf9uqrawIP+erYB8S9IDZiH/XQyn8udDhYnbFU+6V2xypPpmzDVBkUPtNcf+HzLL0gNqP/bHuaG0RlN2tmkbI980rtoH1tlyexdrOkGeTxIjfuP8531z8nYSTU04CmtHfiVw2KLzy6Z6L+WUREcNoiWy+/92ZVq1I3/QfoxPf0UtDh3F31nUfap607vqRWuffkJX09sy8fuOF51klllBD/8Hd5yA31XxsfWrttFmeOB4N8jY98uv9Upe/120VNRmi+N8dKk5RWz06Er0sutR5rEqIue9H5R3mnPi3lOsYFdWNTeYkW6hgMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAH9F1mwj9X3TdPkL/hs5YnbvOYv6UrjtH6P+i6/oI/RM+uk/7O3TDV7Pd7SL0FXTAV5dru43XV/P+rYGZDohvOtMBVxk6oDWcX9v84vbp4Q8Nh4NHdd0xIlSONBF5isWiQZcUkFkRkaCcmMvxDe8RY44R2V0jYixWFxWvSqiu5imzIa0Ma2FI2KphLJmIkL0blofal/0s5S51L1Y02zqzjBu1b5fwwsoO2Hc2w3qT105bza5ycjQsUsjUKy58EkrdRtb7QZcul2c/snGEJeXJQwsKKEi7pwTaphYncm6bIlLD0QN0Iw+VNIHX3Ep2MyVovbxJclp5jb3VjFLHn9m+vgTLlpJKQ9pJmrXXU9UC3nei3zTburCmfV5a2QGHkUbCufqX1p7ITnugXJsPTrXzXXdJgRUFlAnq/JIcKdvHsVKNtFWC6A+zJRSnUahylB2PQXZm+9wypkpUyvHXsnISOx246hxDlXGzkGbk6yk7YHl5KSrfHM3Tdvl4inS1kIuHvTppYQ+sKqBK0OyiMjJ4YDSCTgeMOkEaJNolTA9buYdi10w51HA8AlYbD4rapyuEutnWpRXt89raHTC3sqqbqL/sfHbi20l7p2FNgh5nq0sWu7QAl7dScpWKkNp+HmJ+ZIvj1q+2VGmKlR0sWgL2anIJebzOqnR2bfNxvr39dASMUDKqvSQvhm9JX3bA/hJp73TAcfvspHlVRLy0VR1QTqruePjc9L0chthOJ8J6RKQlU9TMrn+9A0oZEdpLfLc7vfd9n0amKisxKb0+PFmV7mYHrK7J2zsd8NYIKNuOaz/smhk6oPa84+4kSYe2LsyUHiWcixJe3JoOmC6xKm2n44mMQbrbtqxkIUKVB3RAOSoeEOkSIyvz1FJNSs9jVq1KV3TA0dbjmHbKoQvcGAFbtR/1rtwBy4nE8g7YKuHFreiA1bVu6ID2UsnX4toDOmC6fKrcwHUPyVPOQZ0gaR/C6eayquzguWe0t9d75wipKtH92suOegdstnWpXXqzhNe2ogNKII2Aen3w7Xz0U4dTbiZpkOLhVZrjPKQDphmTXYs9WMSOtkskcij6LbpTlXLoQBKdaiPXM32Rnc2jkaRL4ZntJb64ZFeJZKFd+whJJsUIOG3r0rCVyKVLbKOEl7amA+r+hWE7WZAJx+fnp2wzpJTTbxBxD+iAOkCF92Ko0Sd2yfRYCTscSRxZCXlAFR3IplEhomLJRNT89nJRNbaQXl279trD3F7SpzlgRAkJLu2AMyX8KftiLLHWzoOPqN+J2DQ7w/e1s93eLa2v5oo37aeZrdjanjrPaVazXfm6rRe72xB/yOih2/QeEvhJcnEcpr1y81FM/YBnGGYdDH8AAAAAABSqt8R+UvvzB/dU78/c8bUS8LvogPhVdED8KjogfhUdEL9KO6AcZVd9djIZ3s2LiOIDNsWnrsqP4CTD56j6qnscI3b02fokfxrqWHTA8gNeESXmSijqP3z9Ke/n8k6Nn1YeDwlFN/oY+lNe/T50svTpomFT7QgRHEhcDK926FP3kM5RfEhx8pGqzfABCt0qVy6fCNI/00ay+k4JRZIh3/QJP/y+4QiL1rdm9ukATj9YU31stbW6yE06SHSPqpDhg61Z9QmKKo8kR86UUG8k/dVPnFZW+G15fDCtj8+kT//qZ5gtkNTL069ulJfEYUnGynIAmpRY5yq9KkKF3IXbJUhFqw+AphzllaHv5Yw7YD7e+2EalQ6nfUOi+qpdLeLD6OcDUveQMa82mgZKTIRU1QGLTW15poTxqZCSR43phC9l3AH9OqpTunSnUY8n1gnSNbk+0CMz3aP4rkZTnevQAfXGJaZ+cmrY6+oOKLZ2XsUCft+4A9r4IPcBw73j6IJm96M297t3IFOHcHmp+ctHgzrX3AHL6NQBZ0qQ2lejXP21GEEPfCHVwUgLd74Dm752MvMUJRlnHZ2ldadTkMF3yFV/w8ND5T1OMQdsllBFj5ZU/VMJ+FVyeNKlTZ+0WaA4aNoDvAO+F9fkIlkeazbTJ4Gy2ruEP61Lo5Xclg7fm7pO+sJQDRn+8uVUNory9VpcVKBVgiRJ3VW/n+Ch/BRJCogHnrJuUmk8lx4eOdAu4oTdb6iNpPARMD8eLi5wftxdazSMVXrJHrpH+d35egYQ8upqmhdx0nmkB0XcbAna71y+/BbfRI4Y3bhRPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8ynt3jBBe1P7QdRsLXbutvf4lu66L0P/Fxz+2x32nPjR4jI74l/z/OqAczQj9G7rusJWjpJX+i8eKDvjqrLp7HQVfteaXXR+hL6ADvjjpevZq/e81p4Bdt4vQF9ABX10eXb4xzPwsmSRE6AvogPguRsBV6ICPxgi4ytABt73NqT5317Mtq9PuOr7UnU+73Wk0+9p+7iShKZ6MfF53l88Iq4UFhN7jT4dDmYc57Q67IYvkNCpubC9ZnfaxoG6UsNW09U6eNap67tPaXlpAOqC1RIrQv6fDLm8q7Tet/efucNLXugP2Gj0z002l71rtUzbxixs6oD1du8iysOdsuk5FU6pzRHXde8SIdE/qUnvFYvFkf0kBA6tXJCiHlEPE1W8ZvEdk15VdrHCK1d1wZOZKGDIbdvIYMd01IkRj+6F90oVYE3nsRRfbtY84abuiAw55NTvTuvZ5XVLTHDrq0vEkjaC110e+u5O2fD55JSx7e/rQFzthlR2uq25kPNJa73Cx1hi65N0CCpKTJjh8Wh4R+bbRhW7npUWcsMWIzRUr6YrDxRLkGVq7BHs2L6msy8VB1GTTUhvbl+eiD0yyIvqzdMAbtd9d9e9ZVhWRk7YuNEoXMyW8sKGe0txpQV7Psne237pLFilyU9SRESzidL33O20kCywqoCADY4wamjpdZuR4RH+WkA+jQhJEWDMrBudEYqPfyWbpcLZLeJOeEP1ODq29aqZxjuQ9EDPbS6eIkJL6DiXO1t6vARIp+VtQYxttXVjTPq9s2DsJpZa3cBr55QRO14DcyG/Sqr7zRQYSTLtcRErKOCCa550CCpogXZslGCGfRpohUi6v6RCX0QO57ufL/LBeQo0SJJgP3N4jyyylU6b+Nbt9eResiXKzNWsvOaYrgM1PItxq65ImXdg+L22opu5RBG0ikTvF0G0KOXFxvPLpKw05NIScih5YXUDuNJJfhApylY9Qka9FTw5XlaA8dRol5Opm1d4Uec3UcDwCVotZu/Z6HY5gYS5yafu8tGHnyt2UucxwFG83SjH65HaoepTk5cdvbQHDGCAL0zFSBgY/Ap/1ncc0NxliI6SKmrdKmF656gzluMY1fqaGkxEwQjWpvY97kl9R+2n3F99sn9c27Fy1m2Xlm41SjGt536UneKBqHFnyLrqugOq4lNfYLGUxzJvMMI9P0lzOySjjgWYJuWMM6r2RxeihMzUcj4CtOyyRal9Xblp58c32eW0LOmA+YqWiA+ahLLWlnNTFNUtS+LOLdQWMGrh4/JGkLKp8bbwbHfM6gcwYPdAsIa/NJL8iXZHbTA3HI2C18aBd+y93wBvt89qG/a9aYr5/bD8POoHLqYvNUlAOWc2HBAnYq/lOB+wvO69CswPKXePoPtjSllJ0o4RpXcYTrNvba6geAccd8Gbt6w6oD/gt6bhO4mb7xCH6gx0w2sN4jPS2ON5ypDwkgdqdS/CqDqhPJZJ0CMshR5dH16NIPUjRjRLSKD4Yx9zeXkM3RsBW7cs7sLIDTtu6NCo9X4bKR5F/rgPazu1O733flw+sbFK0Hx6RTS9jZkEBhZnDK5ej7nj43PS9HMt0CKv7TJnFTUfACFWaJdRDkBrH5NxmO+D8CNisfbsDRlufq7Yu3Gqf3Um2yiW8ttycRUi0+4ckSXud5yU+2DuPmcyawpBAfLEDygHKh3c4hNXmMgMdNfwoQdIsYfpEY/0leHYEvF/7oQNKfNpyxRxQmnJSwmsb9r9qiWb/KC9GuVHktZdNRd5Czr3RGGTuF1BqN3A5XrQPYWP8HSVImiXkO/ls9JRnyK1dQwnNjoD3a587oLR1vpFb0QFbJby2Yf+rlmj2jzJFapTpiCHKh3yD+wWUmg1c3eHOdMDpBat5AGdKkAv4+JHaaG/yU5nZDjg3AjZrX1cud8BWW1dGpXsVmyW8tmFH7/eP8txOjTLTOI3IBQWUmoe36u4pCxnyyucu0yfJclhah6LdgaZ1r2OGys52wHJSVyZq1l4qV1wuclmttq4sb5/XNrTu/f5RpJCBwsOyy9VjWiNdYvpkfkEBpWYDy+UwX5jkIhVZVBmX166kSpC1O5AMoMP21rElx6KDD3nNdsCysDLR/dpLOBaK2NzWlRXt89KGnat2c64Dxu7ZLZoHNeSKD0HKUtE8cfgk0gPqax1QAml4kfZNWUgwD3rS98cDoEUWo1I6RO0SysN98BIkJh/WYm17+9F4NUrUqL2kT0OgRKaNJTBt69KK9nlpw85Vu9nsH3psrDPp3VxOrY9fkrT32mhxCdnkAyJxHlBf7oCerz9PS1nkcPEsqFIkls4YoZkOpEfOTiUZTrwTyH11rJXr5XDBnNleWkmbYe+ljBLN1N4eXGp5UpTFWQtO27owyjimqbJVo4RXJtWchERZ+aF/SCi8y52vx8kxufb9+XS6WE/0SD9tk+iWEvKAahdQmDm8nqMqs4goE1E1OQsGETdTQpk2xvS4zTdDtee2j4R+K1IlSquEBKe1P2hrRmSrrUur2ueFaU3HITHTP6JfSY+SUyzFDO876IGK4NCAH6mhJBwhMVPAYO7wRr4y8Syz8DNezLX5cEbk6s6VkPtbcTerw+Ioan57vyZ4TeoOOFP76PIy4uURsNXWlXXt85fUezVcdtXoYlF+RemB9tUxze6WtllxQKZJt2sO54207dq3N1hT5GCmff6mYn4uhisI8Ayjq8PwThLwDHLLV174ZPIRIeAppMsN1+BqAXgCu/28nvu3/lNv/r42awa+rHrix/iHX2BjX3ccfQ4ZAAAAAAAAAADg/2Xb/58+K4mX0z3r41pf/GBi9R3d2/jo47+IDohfRQfEr6ID4lfRAfGr6ID4VXRA/Co6IH4VHRC/yjvgSV7K31IRm/jxlfK3BvXHuESZTn9qSxxmvvy099X9uHtcLLrxQ4ZuqE/ZAT22+6jKmitBfzZLHKtfB4liefPndcjhSP1K5CNT/vpURNkRDhFR/hJc+SNBWfoFoe5YdY8i91bHHerTFx2w+Ims4gd6Z0rIP40kIkpExOhUw2+So6EHS8cJPZapP0hwp2E7kB6lY4r97FHxe1Gy8qidVntUowdqjinnonvo4GRjn/boRg+MzPRHACVpdEBNa7/NpuNg/rLzTAlaH+tkNg5alJCgFruVxB6B36cHKI0oetg89DYMMnK04ioW/U+kH16VQSl981OC1cVOSe9NW1gxEdaFCOlYF8GBxMVIrJ0udcCPoasOG90vwX4czUPSGVMWfFv/ZehhK8eT6fQofuFTA6OV0n2Gi2FxzJMySssZghGyTjEuUU6DnKuOdtObkOHXCxeUMJwaDHyvSI7V8EPSshSdrZCP5uT4yQQwQuJzeniryVrZPYovzA9DWFL1HhkDpx1wn3vtbAlFdI6fFoXfVx3u0ZLLcZOb1noyX/VkJcNXhFSeoQ3jl5qWWP+measDSqQP2jMlFJNBlYqQodBe8UrqDtA6Rnmck6RVF5MrcLlcDzuizjr3ijq67o6q7nI3O+CiEnI31d/MsQBeyL3uUHZKSXssblpl/tYXJvfB890jtlDVP+SiJJ9yVviADih3055cEsw+ecQvqQ9WeU3Tf6MgRIwmLka9IoFb3AFrHp0UD3lU2QHjmbO60wGrmayMfJGH3tO0H1jit9THUAa1COnTiyzi/NFcPpxVEjH+52jqSf9sBxz9hFc9r5PUqQMOj6dF6oAzJdQPmotxT58X1gXgd9WHIx99fYcjnuvV88KtjUIWHHWViTrrsgN6oG1uBNT+l/rV3RGwOhc21VzVxu3pwyb8kvoYpkdl0gvylap62KJkE7vd0DmgRcyY7R6j/GpSdHMOWG52twNWJYyfNeowHkH8uvpgpM5Wxk7vjNPaO1P6OuulHVDWp4uuSksyjA2FreuAkzsruc/hpztfRX2w0lIZOxkB81XyzpNdOfDFPXN+H2Jy3zsyrpF3QLneD5mlDjhTwuiUGfVHMZ2v4rfI0RluC2V+lIaWoWtNO2C6VZmuqcisbchatkmJ79yIVv1FFrwDVt02VXOmBLntLUqQsbMcUhUd8HXIURseIOdDmAM2nBSH3qQRRo7/zSGw2FLnXWlBQrduAqQj5ZtY3cp7j2SQu5FEegdcVEKRJsmX4D7ywa+RPpTHAzlUcbsoV7PoBLI2DmC+3MkUKp6zSbJhKLlOjrMkjDiZnhUzsaEcHZ+Ki6iT1d7X/MFL9DsJeUBHutQBZ0rQDaMEDfrt/HtxEz1slCuF3yFXKz2Kl77Xp7T5yiXh7tSfdfiT1Tlq927J8lHT7nk8ybbSFRvHUmMj5326bgu5KEp36ft3y2s6CGnsubfnJRKODqgj3MVj80xhtgTrpJdPr2x0Rc3g49S/Sz9N9zO61kP4LTqa+UgjipvaiJG1qQMW//yILZv4jLtpXFfzajv+EZmGNteaD8YqvVAOV17rTGojkbnXzpRQVDaPsHaSmDT+NqYXeLL3nR5Lm0DlOZbZ6PXJLlq7dOna6+Ax/siBjZLdIV9TR7SD+Jc4cjbGO85pcv11NkZaQZfD0K9PUtRRq3uxT2uHmRK2Wv/uWhXwriP2R7Gfu5u3QwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADW28Rrtum3EcILO3TiPRbwJ/U95+JP6vUcMqeIER+yeI4wXtdOjxsD4F+mR7jrYwEPp0PdIBp6r+G9h/HCmAH+eXqEGQB/jE4hDhY6W0t3H+f95qKBncXipTED/PP0CDMA/hht3fQAcGvziVDcD+O2d22v37leMAP88/QIMwD+mKO0bgTV1iZ/x93kTRHM+8UBkBngn6dHmAEQL8weHjADxI/QI8wAiBfGDBA/R48wAyBeGM8A8XP0CI8HQP3oxocH+6slUIcq1d6eZpnj/AP9zal88N99fEb8VG8X24ZJz99fyo+WHJrd83F7MKFP9SLr/UkX3Mf8x/qWN8Lb2/a9aofjZfXTwiXNs34fxGe5F+18l1a/ODy3yrdPCzUUZQ85bXLJx+IDRvvlbb89FQ139YrPzQDH2bZbo2nhvhe+URjusjYdDYDWzPK6GY6PO8ZHpveT0apx+Prx1u4Sq0t2jZ9TfUx7+FhxYTo70Njv70GTptWs9+U4Y46Ns31FI0jiSZbm1oA5srJ5pvuQ37IemTSXOIw+QL+8+hq9oA3jk0MT11gvdFFz+rQVIVp3pj7Ntq/eoXdHOSssdjTgrMm2QdPavk9KbPaf7xWGu6w9mwOgn0wfcVSih+mh8355jBHj7Kf40ZcGcXx3eWDZnDxmcsOdPiGX4mPQOJ43fd9XnSIGyuN7usaf48Qcn+Ma9+09aNOkKes0c9zGrvkn/grLGyE3Q3dNJ9z2PZ0jxRl/y5ea5+4+yKoYxa+xG/17nJZl2jXV18gl5W+lA/S9rThY0JRdQlflAy37ve1lVuU1SPu+oO3T8BfzvrdzLPt8th4AV2TbpCnzvsfFYbbtv1sY7mq1psUpvQhmfswO/lJ2io3FjMcPOUdOk+/YeceqBrUYmOp5gh/muue9bf20G92uxiyiztOi1Hf2oM2Smqp632yEmZ17e/Mb+PKubs5XmmfJPsS4qheOksVGeG31LdIsKd+G1uk01uiqD2vQUWe52faj6kS5dWyMgaLOeXm2bZbSfK//LCoMd1ljtgfAdLuYDI9kxievRS6altsJUV3ofDjKl7jg84mqnNlS/AStOoTF/MweWMrJ6ZKyWDRXmzZC9P/mWd4awRq+0jyL9sEvUK0Zx3uu1N3q1yVZ1OI2tB24MQCqxfeEjbb3HjjNIU22JkNrQyPbGZblw/sPvsqavTkATrt83OVMH+p4/4+FmzxlOS7Z9Wx63C266pR2HzaZYig7RauLp0b80B5YwtYTw+muzWmktJjmzqUrfizMWt88y/bBm2x8uo5YmuXVt5jFbbhgAFzS6s5LqHbHYpqV9zvQJQPg8oNvCR/ef/BV1patAbDR5X0C1ZoL2Iq7kxRlKcss7MSd9gc7a8vbLrvszVwgdVWVqUX8zB5YwmbnszO98ch/yvIoa2Fn2tw1fXZuV1rfPAv3wWZwd/ZqbfUtZnEb3h0AZ9a12RZlY9iHAcY3+MF2bMkAOM12jiV8dP/Bl7Xa0uJa13wbrFoNb8duUUeZZDFzglm3LM4pP40aF05lZ395DbfEP7MHmq6Z9bQWs8a1mB+Xjd+ExkLbg5pnmtQngLEwY3X1LWJxG1oVbg2AzZzmTHqAZTFz5L/Rr+doukf3H3ydNuW4LS2udYzsgthqeBuuFh07G+/KLOxJy3T2oLHluOhPsuJ9wDHrOuUpYol/Zg80XbsD254sehg1bgR/2hQLUz4ItWqdPah5pvtg7XJnirW6+haxuA3vzgCbOc0Zt70/AZzJYvkMcNKv52i6R/cffJ025bgtLa51jOyC2Gp4O01as/fNpx2skSoLu5yN+vfeIsvnVn6S3bLoDF+/B2Oart2BbabTugu92wg2Qs0/1fYZ1s3z8EHN4/mU+2BVm7m1TlZX3yKWlS++NQDebXu/dMTC2OwMcF9+ajprdawxS7h4382Ckwhf1WpLi2sdo3Xzp62dGC11FjbYlYNPfEq5em4/m1f4qEq3qO/vQYumu9GBx1fwRY1gtboxylj6m6Pzg5pnug9WtTsXhtXVt4jFbfjlAXBR21uJM48Afc8mA6DFtrQ61pglnN/3cTMuPYnwRa22tLjWMVozf0qfnr3U6aePL3yCYI6H4ls/9QMt6x13nkUNLPF396BN0y0+eRc2gp1RcydhetevVevsQc0z3Yd7g5taXX2LWNqGXx0A/bHo3bZfOwNc3K9n2MaL+4+l/nphuMvad9SWFtc6RreHj2qS4Q9+pt3WJvNlFnbjeBpfVHfj98m8ny496Jb4m3swQ9Pd6MDVWLG0Ee4MX77v7dM8PKh5pvtgEfO3t8ar33xnU02rP4nIpm0Y268eAJe2vaebaVzrltUAuLhfz7Ht5/e9GgC95b5RGO6yJh61pcW1jpH1h1bDT+ZPftVvPD1pXoD94yf7/nQ57D5PzQ+j+LVwyRRNWeLv7cEcTbewAy9uhDtTPLsLuj0Le1DzTPfh5nsEYXX1Lf2y8sWXBsDFbe9ZNBKqyQxwebZzLIP5fS/b6fuF4S5r41FbWlzrGC2fP9nBbM1pxhcvzfLOBMNZEeOvCs3RtN/cgzmabuHJu7gRPGJu527OUZLHNE9jALKT7fbou7r605hkWn4MgDN9RFc1c1re9v6QLRZGJgPg8mznWAbz+1629PcLw13WxqO2tLjWMbo9fJTzj7nz0WcqRRaTLjbLzsSZicCYJv3mHszRdAs78OJGiOeg7Z2bnQfUHtI8032IKeDt4tdW36IWlp9qEAsjtqqV0/K295hW5f0OtKr78mznWML5fS8H/+8XhrtabWlxrWN0+waynD/5ZX/8wdz4ynyZhZ8f3fH03o9++mXMD/v4671tlvR7ezBH0y3swIsbIZ3kky+ICst1wdD8kOaZ7kO0zHhWpiRtKm1l9S1uafmRvD0E26pWTiva3se5ySTXcxBlySuynWEJl+379wvDXa22tLjWMVoxf/KjVPWquKCKKgsbkxqmP/xoxdc/8OL0o77VmW8Jv7kHMzTd0pN3eSPE+DXeufQhiGZ5Yw9onuYAFFWufpZk61/vyFesddVvRprmAOhDcHNotzXNnLw1FrW9D9/VSLP1Xmn1r/rhimzbLOH8vlc5f7sw3NVqS4trHaPbw0c9f0pH6nKSqd05fW700zpbs/+1VMdeUtp9njheNdf+8zR8bKbK02K+uwdtmm5pB17RCMWM43jR6XCxa+M5wJzvN09rH2Q0yPnqfrwPmZYXjDXVt+jl5ccIeLxovjIyFbertqKZ06q2T3W9nLXZvDid0Foe9YV4TbYtlvwn+g++xFp01JYW1zpGt28gR/Mn/ycgSweZMEyOnZ84k7lX+jXN0TRw7nOh17q6FvftPWjSdDc68Gj2srAR3PRnqsW1OfOZ8d3mae6DaOU7ynNN9W3NmvLTQOCKQcKWmzmtbPtJWrsYNgbAddlO2RaL9/2bheGu991uN574aFxzLOg1catHb6673aVxVPvTwa9ax/xRzsthVx5kG+cml3zjvX5yI/y2PV/SlET6w6lvVOhxezAxm/XbXndNuufY/UYo7It/VeP6vmbwS77TPG97bYbGPoh9/kdBjumnoaeWVf8r5aeMP6qjdJ7Nya1p+00a5T+GHiKJJwO9WHVIa/M1ntv3bxSGV2e9eq4H2yVx4RubAPCP8WfnsTAx++sCAPDvsw/BND7p5OxObuk7AADwj7GHG+1P+duT3kVvzALAPyme15/S492Q/mHCUTQA/CXxVZCW1r8UDQB/iv9me+3Y/LAKAPxVvWDWBwAAAAAAAAAAAAAAAAAAAAAA8D9kP7Twp75Rp7+Q2PhXj37GDxX21H0A/r90/Jv7ByP+TfYbOdN/JOBn/FBhT90H4P9Lz7Q/OAA+6ydif6iwp+4D8P+lZ9rfGgDtn1B51uzphwp76j4A/196pjED/DJmgMC/TM80ngF+2Q8V9tR9AP6/9Ez7g7fAz5o9/VBhT90H4P9LzzRugb/shwp76j4A/196pnEL/GU/VNhT9wH4/9IzbRgAN6fL4XA4ne5MPrbn00nSHXan882xs7fsDpfTzd/w33+edpbq8ysfyN6cdOvr6T1tfPv28XmFLW0ksXIfNOfYiTv/OMKyAwD8b+mZ5gPguwUH7X9x6d0mJ5Vj80w9xdrs0shvO0n1cWfsrXzGPxiaXLWI2dnT8wpb3Ehi3T5Mkotre3xddACA/zc7NfZvmzivjrvLdZfOseP4jPF/h90cdp87+xfbzeSfZrcpjJBkp0NKF+uyPOQedpfLLg8au1h9xzZlq3XOG5/nZk9PK2x5I63eh7OtNjsRQTHNetEBAP7v7MTwwWFXjHf6ZVRxicWgI+Po3+Lc+Mn1EYtua0PoNZbM5jBKkwaKagTqfextDBUTF0tZfmH27FWxLMaDxxML06gljSS+tA+H6nY2Xbnq6eKiAwDAzh41Pt38Dqo6h9761oOzvSWshko7sacTmIqf/JMv/Htuh1ia59Ol8aMtz1SMbh+fWdjSRvraPkyH661P9qq9sLR3DgCANAA2HpL7/VbzQVTNE8aCmURM+encGCrebA40Giom/J+Jb2we/358ffI/tbC2aSM9bB8i73IvLCLCAGbZqdK+C/Q5YCzcsLV05RBqERFus5vuYyzUfLLUPNMzv2dvp7ExrRq3n1rYjGkjrcvWn0nMVHRyrbLlCAOYZadK+21EP2c/Y+kGO13L+0sfOm99+MM2mcnaJkC3i7Wbx5mJm62rZk9PLWzOpJHWZasR8/W0u+DiJvj+AQAgbp0pdhref0I2PbfTeyizn7XzKcvMWjt7b96W+rxt8kjPTd5BfWphs8aNtC5bny7Ojmi+uphf3jsAAJSdJzMnls2Omm8dbu0TtsOHSUR9Kg+f7zi1Mo/z84ab4+6tu0cftsvbx6cWVrjdSOuy9ceCsTC1sdXlBncOAABlJ8nMKeIjRywkG3/TcWIyl4lPmajjeCbiT/RvufnhFKvY3Ic6JrOnpxbmFjTSumxtRKzfkq/o6vFu3DoAAJSdH7cGwOoc3ed5xW53+nzf9MZOtObN3Cbe0BTlpwz9xrP9tsQSNzefzJ6eWphY1kjrsrWI+YmqP64d1UPNHQAAys6NW7fA5Vnn85rdOPn48dbIOWYiRU53Hmndc3PzyezpqYUtbqR12d65BfbHnI3PMpnGAQCg7MyYOQ3tnC3uq2wa8jGdSNwZAEV8iSGX48uNGcsy/shrpkSrZTl4PLWwxY20Llt/GjFznGI6eetzL+MDAEDdOC/G04rZOcv9ATDmNMMUxDa5+ebrTbb5zITGSqqGu2cWtryR1u2DRsw/q7Qp3u0P84wOAAChp0X7vPDnSsWDd3szoZV0cm432BRkeJ7o48TcPdtdvnl96xl8MlStemZhyxtp3T541Mw+2OA294ZKGB0AAEJPC9GYH9ltWPmU3m/DJjd38YGLOwOgncDFbxE0zvE1rHKND5H4rHV8w/vEwlY00rp98NTNEXAuo8r4AADIA+Dk3PBHVNW7lD4lHL1xGT90Up7b+243OU99ulPeGfqg9NG8+97en615saMxzfNUzRXPKGxpI6l1++DD3GEyzvkG5aRz2QHwI3zzE0DAX6cnwc7PjuGXNdNPh44GRf/yQvF7TL3de3V2ag7ntud2Hc7BfXwWox5/IrduV40K/cmGhXqkaIk88+9ObeOnQnubQNWzp2cWtrCRzLp9SDPD65DL/j3e4K0Gy2UHILacDJXA/4ieAzLO7eN0KDXul/xcrr3746Xi3G7kJaZzjVahbu7zcZU0rSro/MhO/9HsSTyvsGWN5NbtQ7yXO1b+oKBZdAAidlIl4H9EzwEf6dKnxdzkc2xhX53eR3vn0W6mqhPpPB4E5n6Ovc7OHOofE70lfj40xH1fc/ZknlbYskZyK/eh9zvhQT2nTRYcAH9aydsiQKHvZ0a+2n5Zsn5hdm+bvv/yvdi2XzmLeVZhCxtJrd0HSb9oFxYfAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADzK29t/uojQ5le4HnsAAAAASUVORK5CYII='
};
//# sourceMappingURL=config.js.map

/***/ }),

/***/ 69:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContatoService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ons_ons_mobile_login__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_pouchdb__ = __webpack_require__(732);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_pouchdb_find__ = __webpack_require__(738);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_pouchdb_adapter_cordova_sqlite__ = __webpack_require__(742);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_pouchdb_adapter_cordova_sqlite___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_pouchdb_adapter_cordova_sqlite__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ons_ons_mobile_analytics__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_common_http__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__environment_config__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_device__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_app_version__ = __webpack_require__(178);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var header_default = {
    'Content-Type': 'application/x-www-form-urlencoded'
};
var httpOptionsDefault = {
    headers: header_default
};
// const header_json = {
//   'Content-Type':'application/json'};
// const httpOptionsJson = {
//   headers: header_json
// }
var ContatoService = /** @class */ (function () {
    function ContatoService(platform, utilSrv, httpClient, storageSrv, analyticsService, device, appVersion) {
        this.platform = platform;
        this.utilSrv = utilSrv;
        this.httpClient = httpClient;
        this.storageSrv = storageSrv;
        this.analyticsService = analyticsService;
        this.device = device;
        this.appVersion = appVersion;
    }
    ContatoService.prototype.GetDbAdapter = function () {
        if (this.platform.is('cordova')) {
            return 'websql'; //'cordova-sqlite';
        }
        else {
            return 'websql';
        }
    };
    ContatoService.prototype.setUltimaAtualizacao = function () {
        var _this = this;
        this.storageSrv.Gravar('ultimaAtualizacao', new Date())
            .then(function (data) {
            // console.log('returning data ', data)
            return data;
        }, function (err) {
            _this.analyticsService.sendNonFatalCrash("storage.get(ultimaAtualizacao)", err);
            return new Date(0);
        });
    };
    ContatoService.prototype.initContatosDB = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_4_pouchdb__["a" /* default */].plugin(__WEBPACK_IMPORTED_MODULE_6_pouchdb_adapter_cordova_sqlite___default.a);
            __WEBPACK_IMPORTED_MODULE_4_pouchdb__["a" /* default */].plugin(__WEBPACK_IMPORTED_MODULE_5_pouchdb_find__["a" /* default */]);
            _this.contatosDB = new __WEBPACK_IMPORTED_MODULE_4_pouchdb__["a" /* default */]('contatos.db', { adapter: _this.GetDbAdapter() });
            resolve();
        });
    };
    ContatoService.prototype.destroyContatosDB = function (listaContatos) {
        var _this = this;
        var arrayDb = [];
        var teste = [];
        // this.reinitContatosDB();
        listaContatos.forEach(function (element) {
            arrayDb.push(element.ID);
        });
        // console.log('Array db', arrayDb)
        return __WEBPACK_IMPORTED_MODULE_1_rxjs__["Observable"].fromPromise(this.contatosDB.allDocs({ include_docs: true })
            .then(function (docs) {
            docs.rows.forEach(function (element) {
                if (arrayDb.indexOf(element.doc.ID) !== -1) {
                    teste.push({ _id: element.id, _rev: element.doc._rev, _deleted: true });
                }
            });
            // console.log('Array de exclusoes', teste)
            _this.contatosDB.bulkDocs(teste);
        }));
    };
    ContatoService.prototype.destroyGerenciasDB = function () {
        var _this = this;
        var teste = [];
        //this.reinitGerenciasDB();
        return __WEBPACK_IMPORTED_MODULE_1_rxjs__["Observable"].fromPromise(this.gerenciasDB.allDocs({ include_docs: true })
            .then(function (docs) {
            // console.log('destroy gerencia ', docs)
            docs.rows.forEach(function (element) {
                teste.push({ _id: element.id, _rev: element.doc._rev, _deleted: true });
            });
            //    console.log('Array de gerencias exclusoes', teste)
            _this.gerenciasDB.bulkDocs(teste);
        }));
        // return Observable.of('ok');
    };
    ContatoService.prototype.OinitContatosDB = function () {
        return __WEBPACK_IMPORTED_MODULE_1_rxjs__["Observable"].fromPromise(this.initContatosDB());
    };
    ContatoService.prototype.reinitContatosDB = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_4_pouchdb__["a" /* default */].plugin(__WEBPACK_IMPORTED_MODULE_6_pouchdb_adapter_cordova_sqlite___default.a);
            __WEBPACK_IMPORTED_MODULE_4_pouchdb__["a" /* default */].plugin(__WEBPACK_IMPORTED_MODULE_5_pouchdb_find__["a" /* default */]);
            return new __WEBPACK_IMPORTED_MODULE_4_pouchdb__["a" /* default */]('contatos.db', { adapter: _this.GetDbAdapter() })
                .destroy()
                .then(function () {
                return new __WEBPACK_IMPORTED_MODULE_4_pouchdb__["a" /* default */]('contatos.db', { adapter: _this.GetDbAdapter() });
            })
                .then(function (ContatosDB) {
                _this.contatosDB = ContatosDB;
                resolve();
            }).catch(function (error) {
                _this.analyticsService.sendNonFatalCrash("reinitContatosDB()", error);
                reject(error);
            });
        });
    };
    ContatoService.prototype.reinitGerenciasDB = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_4_pouchdb__["a" /* default */].plugin(__WEBPACK_IMPORTED_MODULE_6_pouchdb_adapter_cordova_sqlite___default.a);
            __WEBPACK_IMPORTED_MODULE_4_pouchdb__["a" /* default */].plugin(__WEBPACK_IMPORTED_MODULE_5_pouchdb_find__["a" /* default */]);
            return new __WEBPACK_IMPORTED_MODULE_4_pouchdb__["a" /* default */]('gerencias.db', { adapter: _this.GetDbAdapter() })
                .destroy()
                .then(function () {
                return new __WEBPACK_IMPORTED_MODULE_4_pouchdb__["a" /* default */]('gerencias.db', { adapter: _this.GetDbAdapter() });
            })
                .then(function (GerenciasDB) {
                _this.gerenciasDB = GerenciasDB;
                resolve();
            }).catch(function (error) {
                _this.analyticsService.sendNonFatalCrash("reinitGerenciasDB()", error);
                reject(error);
            });
        });
    };
    ContatoService.prototype.initGerenciaDB = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_4_pouchdb__["a" /* default */].plugin(__WEBPACK_IMPORTED_MODULE_6_pouchdb_adapter_cordova_sqlite___default.a);
            try {
                _this.gerenciasDB = new __WEBPACK_IMPORTED_MODULE_4_pouchdb__["a" /* default */]('gerencias.db', { adapter: _this.GetDbAdapter() });
                resolve();
            }
            catch (error) {
                _this.analyticsService.sendNonFatalCrash("initGerenciaDB()", error);
                reject(error);
            }
        });
    };
    ContatoService.prototype.loadContatos = function (filtro) {
        var _this = this;
        this.OinitContatosDB();
        return __WEBPACK_IMPORTED_MODULE_1_rxjs__["Observable"].fromPromise(this.contatosDB.allDocs({ include_docs: true })
            .then(function (docs) {
            _this.contatos = docs.rows.map(function (row) {
                if ((row.doc.Nome === filtro) || (filtro = undefined) || (filtro == null)) {
                    return row.doc;
                }
            });
            _this.contatos = _this.contatos.sort(_this.ordernarPorNome);
            return _this.contatos;
        }));
    };
    ContatoService.prototype.getListaContatos = function () {
        var _this = this;
        this.initContatosDB();
        // console.log('fazendo a carga de base inicial ');
        console.log("Inicia Contatos");
        return __WEBPACK_IMPORTED_MODULE_1_rxjs__["Observable"].fromPromise(this.contatosDB.allDocs({ include_docs: true })
            .then(function (docs) {
            console.log('docs lidos de contatos ', docs);
            _this.contatos = docs.rows.map(function (row) {
                return row.doc;
            });
            _this.contatos = _this.contatos.sort(_this.ordernarPorNome);
            return _this.contatos;
        }));
    };
    ContatoService.prototype.getContatoPhoto = function (id) {
        var httpOptions = { headers: new __WEBPACK_IMPORTED_MODULE_8__angular_common_http__["d" /* HttpHeaders */]({ "responseType": 'json' }) };
        return this.httpClient.get(__WEBPACK_IMPORTED_MODULE_9__environment_config__["a" /* Config */].INTRANET_URL + "/fotos?id=" + id, httpOptions)
            .map(function (data) {
            return data;
        });
    };
    ContatoService.prototype.registrarFirebase = function () {
        // const headers =  new HttpHeaders({   'Content-Type':  'application/x-www-form-urlencoded' });
        //  const options = {headers};
        var _this = this;
        this.appVersion.getVersionNumber().then(function (x) {
            _this.appv = x;
        });
        var body = {
            "appVersao": this.appv,
            "dispositivoId": this.device.uuid,
            "dispositivoDetalhe": {
                "so": this.device.platform,
                "modelo": this.device.model
            },
            "topicos": [
                "Publicações"
            ]
        };
        console.log(JSON.stringify(body));
        this.httpClient.post("http://localhost:51078/api/pushnotifications/registrar", body, httpOptionsDefault)
            .subscribe(function (data) {
            console.log('Registro', data);
            return data;
        });
        //   .((err:Response) => {
        //     let details = err.json();
        //     console.log('Detalhes do erro:', details);
        //     return Observable.throw(details);
        //  });
    };
    ContatoService.prototype.getContatos = function () {
        var _this = this;
        if (!this.contatos) {
            return this.contatosDB.allDocs({ include_docs: true })
                .then(function (docs) {
                _this.contatos = docs.rows.map(function (row) {
                    return row.doc;
                });
                _this.contatos = _this.contatos.sort(_this.ordernarPorNome);
                return _this.contatos;
            });
        }
        else {
            this.contatos = this.contatos.sort(this.ordernarPorNome);
            return Promise.resolve(this.contatos);
        }
    };
    ContatoService.prototype.getContatosPorGerencia = function (gerencia) {
        var _this = this;
        if (!this.contatos) {
            return this.contatosDB.find({ selector: { Gerencia: gerencia.Nome } })
                .then(function (result) {
                _this.contatos = result.docs;
                return _this.contatos.sort(_this.ordernarPorNome);
            });
        }
        else {
            var retorno = this.contatos.filter(function (data) {
                return data.Gerencia == gerencia.Nome;
            });
            return Promise.resolve(retorno.sort(this.ordernarPorNome));
        }
    };
    ContatoService.prototype.getGerencias = function () {
        var _this = this;
        //   if (!this.gerencias) {
        return __WEBPACK_IMPORTED_MODULE_1_rxjs__["Observable"].fromPromise(this.gerenciasDB.allDocs({ include_docs: true })
            .then(function (docs) {
            _this.gerencias = docs.rows.map(function (row) {
                return row.doc;
            });
            _this.gerencias = _this.gerencias.sort(_this.ordernarPorNome);
            return _this.gerencias;
        })
            .catch(function (err) { _this.analyticsService.sendNonFatalCrash("getGerencias()", err); }));
    };
    ContatoService.prototype.ordernarPorNome = function (a, b) {
        if (a.Nome < b.Nome)
            return -1;
        if (a.Nome > b.Nome)
            return 1;
        return 0;
    };
    ContatoService.prototype.getContatosSharePoint = function (dataAtualizacao) {
        var _this = this;
        // console.log('httpOptions  ', httpOptions)
        if (dataAtualizacao == null || dataAtualizacao == undefined) {
            dataAtualizacao = new Date(0);
        }
        return this.httpClient.get(__WEBPACK_IMPORTED_MODULE_9__environment_config__["a" /* Config */].INTRANET_URL + "/modificados?data=2018-01-01&tipo=completo", httpOptionsDefault)
            .map(function (data) {
            _this.setUltimaAtualizacao();
            // console.log('dados do sharepoint', data)
            return data;
        }, function (err) {
            _this.analyticsService.sendNonFatalCrash("getContatosSharePoint()", err);
            _this.utilSrv.alerta(__WEBPACK_IMPORTED_MODULE_9__environment_config__["a" /* Config */].MensagemGenerica.MENSAGEM_ALERTA_CONTATOS_SHAREPOINT, 5000, 'top', 'Leitura de contatos');
            return false;
        });
    };
    ContatoService.prototype.getGerenciasSharePoint = function () {
        return this.httpClient.get(__WEBPACK_IMPORTED_MODULE_9__environment_config__["a" /* Config */].INTRANET_URL + "/Gerencias", httpOptionsDefault)
            .map(function (data) {
            return data;
        });
    };
    ContatoService.prototype.bulkInsertContatosDB = function (contatos) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var that = _this;
            // console.log('contatos DB on insert ', this.contatosDB)
            // console.log(contatos)
            return _this.contatosDB.bulkDocs(contatos)
                .then(function (x) {
                that.contatosDB.allDocs(function (err, response) {
                    if (err) {
                        // console.log('erro de insert de contatos ', err)
                        that.analyticsService.sendNonFatalCrash("bulkInsertContatosDB()", err);
                        return err;
                    }
                    else {
                        return x;
                    }
                });
                resolve();
            }).catch(function (error) {
                _this.analyticsService.sendNonFatalCrash("insertTmpContatosDB()", error);
                reject(error);
            });
        });
    };
    ContatoService.prototype.bulkInsertGerenciasDB = function (gerencias) {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_1_rxjs__["Observable"].fromPromise(new Promise(function (resolve, reject) {
            return _this.gerenciasDB.bulkDocs(gerencias)
                .then(function (x) {
                resolve();
            }).catch(function (error) {
                _this.analyticsService.sendNonFatalCrash("insertGerenciasDB()", error);
                reject(error);
            });
        }));
    };
    ContatoService.prototype.clearAll = function () {
        var _this = this;
        this.reinitContatosDB()
            .then(function () {
            _this.analyticsService.sendCustomEvent("Clear Contatos");
            _this.reinitGerenciasDB()
                .then(function () {
                _this.analyticsService.sendCustomEvent("Clear Gerencia");
                _this.storageSrv.limparBase();
                _this.analyticsService.sendCustomEvent("Clear Storage");
            });
        });
    };
    ContatoService.prototype.ValidarGerencia = function (contato, objFiltro) {
        var result = false;
        // console.log('objFiltro ', objFiltro);
        if (objFiltro.filtroGerenciaExecutiva == 'interesses') {
            // console.log('contato ', contato);
            // console.log('log ', contato.Interesse.toLowerCase().trim().indexOf(objFiltro.filtroGerencia.toLowerCase().trim()));
            result = (contato.Interesse.toLowerCase().trim().indexOf(objFiltro.filtroGerencia.toLowerCase().trim()) !== -1);
        }
        else {
            result = ((contato.Gerencia.toLowerCase().trim() === objFiltro.filtroGerencia.toLowerCase().trim()) || (objFiltro.filtroGerencia == ''));
        }
        return result;
    };
    ContatoService.prototype.getImagemPerfil = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.httpClient.get(__WEBPACK_IMPORTED_MODULE_9__environment_config__["a" /* Config */].INTRANET_URL + "/getcontatoimage?id=" + id, { responseType: 'text' })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    ContatoService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["u" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_0__ons_ons_mobile_login__["k" /* UtilService */],
            __WEBPACK_IMPORTED_MODULE_8__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_0__ons_ons_mobile_login__["h" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_7__ons_ons_mobile_analytics__["a" /* AnalyticsService */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_device__["a" /* Device */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_app_version__["a" /* AppVersion */]])
    ], ContatoService);
    return ContatoService;
}());

//# sourceMappingURL=contatos.service.js.map

/***/ })

},[393]);
//# sourceMappingURL=main.js.map